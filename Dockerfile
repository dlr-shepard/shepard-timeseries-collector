FROM eclipse-temurin:21.0.6_7-jre

WORKDIR /app

RUN mkdir /config
COPY target/shepard-timeseries-collector-jar-with-dependencies.jar stc.jar

CMD ["java", "-jar", "stc.jar", "-c", "/config"]
