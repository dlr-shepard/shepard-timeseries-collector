# This template uses jdk17 for verifying and deploying images
image: maven:3-eclipse-temurin-21

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED == "true"

stages:
  - test
  - build
  - deploy

.version_number:
  script:
    - export VERSION_NUMBER=$(echo ${CI_PIPELINE_CREATED_AT} | cut -d'T' -f 1 | tr - .)

# Verify merge requests
test:
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS -DskipITs verify'
    - 'cat target/site/jacoco/index.html | grep -oP "Total.*?([0-9]{1,3})%"'
  coverage: '/Total.*?([0-9]{1,3})%/'
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - target/jacoco.exec
      - target/surefire-reports/*
      - target/site/jacoco/*
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

integration-test:
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS -DskipUTs verify'
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - target/failsafe-reports/*
    reports:
      junit:
        - target/failsafe-reports/TEST-*.xml
    
coverage:
  stage: build
  image: haynes/jacoco2cobertura:1.0.10
  needs: [test]
  dependencies: [test]
  script:
    - 'ls -hal target/site/jacoco'
    # convert report from jacoco to cobertura
    - 'python /opt/cover2cover.py target/site/jacoco/jacoco.xml src/main/java > target/site/cobertura.xml'
    # read the <source></source> tag and prepend the path to every filename attribute
    - 'python /opt/source2filename.py target/site/cobertura.xml'
  artifacts:
    expire_in: 1 week
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml

# Build jar
build:
  stage: build
  before_script:
    - !reference [.version_number, script]
    - export VERSION_NUMBER_DEV=${VERSION_NUMBER}-dev-${CI_PIPELINE_IID}-SNAPSHOT
    - echo ${VERSION_NUMBER_DEV}
  script:
    - mvn ${MAVEN_CLI_OPTS} -Drevision=${VERSION_NUMBER_DEV}  package -DskipTests
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 week

.docker:
  image: docker:27.5
  stage: deploy
  needs: [build]
  dependencies: [build]
  services:
    - name: docker:dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" ${CI_REGISTRY}
    - !reference [.version_number, script]
    - export VERSION_NUMBER_DEV=${VERSION_NUMBER}-dev.${CI_PIPELINE_IID}
    - echo ${VERSION_NUMBER_DEV}
  script:
    - docker build -t ${CI_REGISTRY_IMAGE_MOD}:latest -t ${CI_REGISTRY_IMAGE_MOD}:${VERSION_NUMBER_DEV} .
    - docker push ${CI_REGISTRY_IMAGE_MOD}:latest
    - docker push ${CI_REGISTRY_IMAGE_MOD}:${VERSION_NUMBER_DEV}

docker-old:
  extends: .docker
  variables:
    CI_REGISTRY_IMAGE_MOD: ${CI_REGISTRY_IMAGE}/shepard-0.9
  rules:
    - if: $CI_COMMIT_BRANCH == "shepard-0.9"

docker:
  extends: .docker
  variables:
    CI_REGISTRY_IMAGE_MOD: ${CI_REGISTRY_IMAGE}
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  

# Build site
site:
  stage: build
  needs: [test]
  dependencies: [test]
  before_script:
    - !reference [.version_number, script]
    - export VERSION_NUMBER_DEV=${VERSION_NUMBER}-dev-${CI_PIPELINE_IID}-SNAPSHOT
    - echo ${VERSION_NUMBER_DEV}
  script:
    - 'mvn $MAVEN_CLI_OPTS -Drevision=${VERSION_NUMBER_DEV} site'
    - 'ls -hal target/site'
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/site/*
  only:
    - main

# Publish reports to gitlab pages
pages:
  stage: deploy
  needs: [site]
  dependencies: [site]
  script:
    - 'mkdir public'
    - 'mv target/site/* public/'
    - 'echo ${CI_PAGES_DOMAIN}'
  artifacts:
    expire_in: 1 week
    paths:
      - public
  only:
    - main
