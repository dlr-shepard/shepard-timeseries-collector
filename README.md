# shepard Timeseries Collector (sTC)

[Download JAR](https://gitlab.com/dlr-shepard/shepard-timeseries-collector/-/jobs/artifacts/main/raw/target/shepard-timeseries-collector-jar-with-dependencies.jar?job=build)

[Documentation](https://gitlab.com/dlr-shepard/shepard-timeseries-collector/-/blob/main/docs/README.md)

[Maven Reports](https://dlr-shepard.gitlab.io/shepard-timeseries-collector/)

[Recommended JRE](https://adoptium.net/de/)

## Logging configuration

By default, sTC logs to the console with log level `info`.

Using the command line interface, the default log level can be specified using application parameter `-l <level>` with `<level>` being `debug`, `info`, `warn` or `error`.

Alternatively, a custom logback configuration file can be specified JVM parameter `-Dlogback.configurationFile=<file>`. In this case, no log level should be specified using the cli parameter `-l`. An exemplary configuration file that logs to the console as well as to a file in the `log/` folder is available in the `logback-file.xml` file.
