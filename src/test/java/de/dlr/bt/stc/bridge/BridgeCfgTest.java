package de.dlr.bt.stc.bridge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

public class BridgeCfgTest extends BaseTestCase {

	@Mock
	private HierarchicalConfiguration<?> config;

	@Test
	void testGetSourceId() {
		when(config.getString("source_id")).thenReturn("source");
		var cfg = new BridgeCfg(config);
		assertEquals("source", cfg.getSourceId());
	}

	@Test
	void testGetSinkId() {
		when(config.getString("sink_id")).thenReturn("sink");
		var cfg = new BridgeCfg(config);
		assertEquals("sink", cfg.getSinkId());
	}

	@Test
	void testGetQueueSize() {
		when(config.getInt("queue_size", -1)).thenReturn(123);
		var cfg = new BridgeCfg(config);
		assertEquals(123, cfg.getQueueSize());
	}

	@Test
	void testGetQueueDuration() {
		when(config.getInt("queue_duration", -1)).thenReturn(123);
		var cfg = new BridgeCfg(config);
		assertEquals(123, cfg.getQueueDuration());
	}

	@Test
	@SuppressWarnings("unchecked")
	void testGetMapping() {
		var mapping = mock(HierarchicalConfiguration.class);
		when(mapping.getKeys()).thenReturn(List.of("a", "b", "c").iterator(), List.of("a", "b", "c").iterator());
		when(config.configurationAt("mapping")).thenReturn(mapping);
		var cfg = new BridgeCfg(config);
		var expected = new MappingCfg(mapping).toString();
		var actual = cfg.getMapping().toString();
		assertEquals(expected, actual);
	}

	@Test
	void testGetValueTemplate() {
		when(config.getString("value_template", "value")).thenReturn("valueTemplate");
		var cfg = new BridgeCfg(config);
		assertEquals("valueTemplate", cfg.getValueTemplate());
	}

	@Test
	void testGetTimestampTemplate() {
		when(config.getString("timestamp_template", "")).thenReturn("timestampTemplate");
		var cfg = new BridgeCfg(config);
		assertEquals("timestampTemplate", cfg.getTimestampTemplate());
	}

}
