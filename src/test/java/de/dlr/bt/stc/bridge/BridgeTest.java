package de.dlr.bt.stc.bridge;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.entities.DataPoint;
import de.dlr.bt.stc.entities.Mapping;
import de.dlr.bt.stc.eventbus.CacheFullEvent;
import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.task.Result;
import de.dlr.bt.stc.task.ResultStatus;
import de.dlr.bt.stc.util.DateHelper;

class BridgeTest extends BaseTestCase {

	@Mock
	private EventBus eventBus;

	@Mock
	private DateHelper dateHelper;

	@Mock
	private BridgeCfg cfg;

	@Captor
	private ArgumentCaptor<CacheFullEvent> cacheFullEventCaptor;

	@BeforeEach
	void initCfg() {
		var mapping = mock(MappingCfg.class);
		when(mapping.getMeasurement()).thenReturn("meas");
		when(mapping.getLocation()).thenReturn("loc");
		when(mapping.getDevice()).thenReturn("dev");
		when(mapping.getSymbolicName()).thenReturn("symName");
		when(mapping.getField()).thenReturn("field");
		when(cfg.getSourceId()).thenReturn("source");
		when(cfg.getSinkId()).thenReturn("sink");
		when(cfg.getMapping()).thenReturn(mapping);
		when(cfg.getValueTemplate()).thenReturn("value");
		when(cfg.getTimestampTemplate()).thenReturn("");
	}

	@ParameterizedTest
	@CsvSource({ "20,-1", "-1,20", "20,20" })
	void initializeTaskTest(int queueSize, int queueDuration) throws ConfigurationException {
		when(cfg.getQueueSize()).thenReturn(queueSize);
		when(cfg.getQueueDuration()).thenReturn(queueDuration);
		var bridge = new Bridge("key", cfg, eventBus);
		assertDoesNotThrow(() -> bridge.initializeTask());
	}

	@ParameterizedTest
	@CsvSource({ "-20,-1", "-1,-20", "-1,-1" })
	void initializeTaskTest_InvalidConfiguration(int queueSize, int queueDuration) throws ConfigurationException {
		when(cfg.getQueueSize()).thenReturn(queueSize);
		when(cfg.getQueueDuration()).thenReturn(queueDuration);
		var bridge = new Bridge("key", cfg, eventBus);

		Result result = bridge.initializeTask();
		assertTrue(result.isNotOk());
		assertEquals("Bridge key is misconfigured", result.getErrorMsg());
		assertEquals(ResultStatus.CONFIG_INVALID, result.getStatus());
		assertTrue(result.getException() instanceof ConfigurationException);
	}

	@Test
	void startTaskTest() {
		var bridge = new Bridge("key", cfg, eventBus);
		bridge.startTask();
		verify(eventBus).register(bridge);
	}

	@Test
	void stopTaskTest() {
		var bridge = new Bridge("key", cfg, eventBus);
		bridge.stopTask();
		verify(eventBus).unregister(bridge);
	}

	@Test
	void cacheFullTest_queueSize() {
		when(cfg.getQueueSize()).thenReturn(2);
		when(cfg.getSourceId()).thenReturn("source");
		var bridge = new Bridge("test", cfg, eventBus);
		for (var i = 0; i < 2; i++) {
			assertFalse(bridge.cacheFull());
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Collections.emptyMap()));
		}
		assertTrue(bridge.cacheFull());
	}

	@Test
	void cacheFullTest_queueDuration() throws IllegalAccessException {
		when(cfg.getQueueDuration()).thenReturn(2);
		when(cfg.getSourceId()).thenReturn("source");
		when(dateHelper.getDate()).thenReturn(new Date(1), new Date(2), new Date(3));
		var bridge = new Bridge("test", cfg, eventBus);
		FieldUtils.writeField(bridge, "dateHelper", dateHelper, true);
		for (var i = 0; i < 2; i++) {
			assertFalse(bridge.cacheFull());
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Collections.emptyMap()));
		}
		assertTrue(bridge.cacheFull());
	}

	@Test
	void cacheFullTest_wrongSource() {
		when(cfg.getQueueSize()).thenReturn(1);
		when(cfg.getSourceId()).thenReturn("source");
		var bridge = new Bridge("test", cfg, eventBus);
		assertFalse(bridge.cacheFull());
		bridge.onDataAvailableEvent(new DataAvailableEvent("different", 1, 1l, Collections.emptyMap()));
		assertFalse(bridge.cacheFull());
	}

	@Test
	void clearCacheTest() {
		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 5; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Collections.emptyMap()));
			data.add(new DataPoint(i, i));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		assertEquals(expected, cacheFullEventCaptor.getValue());
	}

	@Test
	void clearCacheTest_variablesSpaces() {
		var mapping = mock(MappingCfg.class);
		when(mapping.getMeasurement()).thenReturn("{{test}}");
		when(mapping.getLocation()).thenReturn("{{ test}}");
		when(mapping.getDevice()).thenReturn("{{test }}");
		when(mapping.getSymbolicName()).thenReturn("{{ test }}");
		when(mapping.getField()).thenReturn(" {{ test }}");
		when(cfg.getMapping()).thenReturn(mapping);

		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 5; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Map.of("test", "testValue")));
			data.add(new DataPoint(i, i));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data,
				new Mapping("testValue", "testValue", "testValue", "testValue", " testValue"));
		assertEquals(expected, cacheFullEventCaptor.getValue());
	}

	@Test
	void clearCacheTest_variablesText() {
		var mapping = mock(MappingCfg.class);
		when(mapping.getMeasurement()).thenReturn("meas_{{ test }}");
		when(mapping.getLocation()).thenReturn("loc{{ test }}_");
		when(mapping.getDevice()).thenReturn("{{ test }}device");
		when(mapping.getSymbolicName()).thenReturn("sym{{ test }}Name");
		when(mapping.getField()).thenReturn("{{ test }}field");
		when(cfg.getMapping()).thenReturn(mapping);

		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 5; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Map.of("test", "testValue")));
			data.add(new DataPoint(i, i));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data, new Mapping("meas_testValue", "loctestValue_",
				"testValuedevice", "symtestValueName", "testValuefield"));
		assertEquals(expected, cacheFullEventCaptor.getValue());
	}

	@Test
	void clearCacheTest_multipleVariables() {
		var mapping = mock(MappingCfg.class);
		when(mapping.getMeasurement()).thenReturn("meas{{ test }}{{ test2 }}");
		when(mapping.getLocation()).thenReturn("{{ test2 }}loc{{ test }}");
		when(mapping.getDevice()).thenReturn("{{ test }}device{{ test }}");
		when(mapping.getSymbolicName()).thenReturn("sym{{ test }}Name{{ test2 }}");
		when(mapping.getField()).thenReturn("{{ test }}{{ wrong }}");
		when(cfg.getMapping()).thenReturn(mapping);

		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 5; i++) {
			bridge.onDataAvailableEvent(
					new DataAvailableEvent("source", i, i, Map.of("test", "testValue", "test2", "testValue2")));
			data.add(new DataPoint(i, i));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data, new Mapping("meastestValuetestValue2", "testValue2loctestValue",
				"testValuedevicetestValue", "symtestValueNametestValue2", "testValueundefined"));
		assertEquals(expected, cacheFullEventCaptor.getValue());
	}

	@Test
	void clearCacheTest_variablesErrors() {
		var mapping = mock(MappingCfg.class);
		when(mapping.getMeasurement()).thenReturn("{{meas");
		when(mapping.getLocation()).thenReturn("loc}}");
		when(mapping.getDevice()).thenReturn("}}dev{{ test }}");
		when(mapping.getSymbolicName()).thenReturn("{{ test }}}}");
		when(mapping.getField()).thenReturn("{{ test }}{{");
		when(cfg.getMapping()).thenReturn(mapping);

		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 5; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i, i, Map.of("test", "testValue")));
			data.add(new DataPoint(i, i));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data,
				new Mapping("{{meas", "loc}}", "}}devtestValue", "testValue}}", "testValue{{"));
		assertEquals(expected, cacheFullEventCaptor.getValue());
	}

	@Test
	void clearCacheTest_valueTemplate() {
		when(cfg.getValueTemplate()).thenReturn("$floor($abs(value))");
		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = -2; i < 3; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i / 2d, i, Collections.emptyMap()));
			data.add(new DataPoint(i, (long) Math.floor(Math.abs(i / 2d))));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		var actual = cacheFullEventCaptor.getValue();
		assertEquals(expected, actual);
	}

	@ParameterizedTest
	@ValueSource(strings = { "", "$wrong(value)" })
	void clearCacheTest_valueTemplateInvalidTemplate(String template) {
		when(cfg.getValueTemplate()).thenReturn(template);
		var bridge = new Bridge("test", cfg, eventBus);
		for (var i = -2; i < 3; i++) {
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", i / 2d, i, Collections.emptyMap()));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", Collections.emptyList(),
				new Mapping("meas", "loc", "dev", "symName", "field"));
		var actual = cacheFullEventCaptor.getValue();
		assertEquals(expected, actual);
	}

	@Test
	void clearCacheTest_timestampTemplate() {
		when(cfg.getTimestampTemplate()).thenReturn("$eval(value).timestamp");
		var bridge = new Bridge("test", cfg, eventBus);
		var data = new ArrayList<DataPoint>();
		for (var i = 0; i < 3; i++) {
			var valueString = "{\"value\":" + i + ", \"timestamp\":" + i + "}";
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", valueString, i * 2, Collections.emptyMap()));
			data.add(new DataPoint(i, valueString));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		var actual = cacheFullEventCaptor.getValue();
		assertEquals(expected, actual);
	}

	@Test
	void clearCacheTest_timestampTemplateInvalidTemplate() {
		when(cfg.getTimestampTemplate()).thenReturn("$wrong(value)");
		var bridge = new Bridge("test", cfg, eventBus);
		for (var i = 0; i < 3; i++) {
			var valueString = "{\"value\":" + i + ", \"timestamp\":" + i + "}";
			bridge.onDataAvailableEvent(new DataAvailableEvent("source", valueString, i, Collections.emptyMap()));
		}
		bridge.clearCache();
		verify(eventBus).post(cacheFullEventCaptor.capture());
		var expected = new CacheFullEvent("sink", Collections.emptyList(),
				new Mapping("meas", "loc", "dev", "symName", "field"));
		var actual = cacheFullEventCaptor.getValue();
		assertEquals(expected, actual);
	}
}
