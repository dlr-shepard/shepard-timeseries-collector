package de.dlr.bt.stc.task;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TaskProviderStateManagerTest {

	private ITaskProvider provider;

	@BeforeEach
	public void initMock() {
		provider = mock(ISourceProvider.class);
	}

	@Test
	void testGetResults() {
		var state = getStopped();
		var actual = state.getResults();
		var expected = Stream.of(ResultAction.INITIALIZE, ResultAction.START, ResultAction.STOP)
				.map(a -> new ProviderResult("id", a)).toList();
		assertTrue(actual.containsAll(expected));
	}

	@Test
	void initialize_ok() {
		var state = new TaskProviderStateManager(provider);
		var result = new ProviderResult("id", ResultAction.INITIALIZE);
		when(provider.initializeTasks()).thenReturn(result);

		var actual = state.initialize();
		verify(provider).initializeTasks();
		assertTrue(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void initialize_nok() {
		var state = new TaskProviderStateManager(provider);
		var result = new ProviderResult("id", ResultAction.INITIALIZE);
		result.getTaskResults().add(Result.error("errorID"));
		when(provider.initializeTasks()).thenReturn(result);

		var actual = state.initialize();
		verify(provider).initializeTasks();
		assertFalse(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void initialize_exception() {
		var state = new TaskProviderStateManager(provider);
		var exception = new RuntimeException();
		doThrow(exception).when(provider).initializeTasks();

		var actual = state.initialize();
		verify(provider).initializeTasks();
		assertFalse(actual);

		assertTrue(checkExceptionInList(
				state.getResults().stream().flatMap(result -> result.getTaskResults().stream()).toList(), exception));
	}

	@Test
	void start_ok() {
		var state = getInitialized();
		var result = new ProviderResult("id", ResultAction.START);
		when(provider.startTasks()).thenReturn(result);

		var actual = state.start();
		verify(provider).startTasks();
		assertTrue(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void start_ok_when_stopped() {
		var state = getStopped();
		var result = new ProviderResult("id", ResultAction.START);
		when(provider.startTasks()).thenReturn(result);

		var actual = state.start();
		verify(provider, times(2)).startTasks();
		assertTrue(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void start_nok() {
		var state = getInitialized();
		var result = new ProviderResult("id", ResultAction.START);
		result.getTaskResults().add(Result.error("errorID", new RuntimeException()));
		when(provider.startTasks()).thenReturn(result);

		var actual = state.start();
		verify(provider).startTasks();
		assertFalse(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void start_exception() {
		var state = getInitialized();
		var exception = new RuntimeException();
		doThrow(exception).when(provider).startTasks();

		var actual = state.start();
		verify(provider).startTasks();
		assertFalse(actual);
		assertTrue(checkExceptionInList(
				state.getResults().stream().flatMap(result -> result.getTaskResults().stream()).toList(), exception));
	}

	@Test
	void stop_ok() {
		var state = getStarted();
		var result = new ProviderResult("id", ResultAction.STOP);
		when(provider.stopTasks()).thenReturn(result);

		var actual = state.stop();
		verify(provider).stopTasks();
		assertTrue(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void stop_nok() {
		var state = getStarted();
		var result = new ProviderResult("id", ResultAction.STOP);
		result.getTaskResults().add(Result.error("errorID", new RuntimeException()));
		when(provider.stopTasks()).thenReturn(result);

		var actual = state.stop();
		verify(provider).stopTasks();
		assertFalse(actual);
		assertTrue(state.getResults().contains(result));
	}

	@Test
	void stop_exception() {
		var state = getStarted();
		var exception = new RuntimeException();
		doThrow(exception).when(provider).stopTasks();

		var actual = state.stop();
		verify(provider).stopTasks();
		assertFalse(actual);
		assertTrue(checkExceptionInList(
				state.getResults().stream().flatMap(result -> result.getTaskResults().stream()).toList(), exception));
	}

	@Test
	void cleanup_ok() {
		var state = getStopped();

		var actual = state.cleanup();
		verify(provider).cleanupTasks();
		assertTrue(actual);
	}

	@Test
	void cleanup_exception() {
		var state = getStopped();
		var exception = new RuntimeException();
		doThrow(exception).when(provider).cleanupTasks();

		var actual = state.cleanup();
		verify(provider).cleanupTasks();
		assertTrue(actual);
	}

	private static boolean checkExceptionInList(Collection<Result> results, Throwable exception) {
		return results.stream().map(Result::getException).anyMatch(exception::equals);
	}

	private TaskProviderStateManager getCreated() {
		return new TaskProviderStateManager(provider);
	}

	private TaskProviderStateManager getInitialized() {
		var state = getCreated();
		when(provider.initializeTasks()).thenReturn(new ProviderResult("id", ResultAction.INITIALIZE));
		state.initialize();
		return state;
	}

	private TaskProviderStateManager getStarted() {
		var state = getInitialized();
		when(provider.startTasks()).thenReturn(new ProviderResult("id", ResultAction.START));
		state.start();
		return state;
	}

	private TaskProviderStateManager getStopped() {
		var state = getStarted();
		when(provider.stopTasks()).thenReturn(new ProviderResult("id", ResultAction.STOP));
		state.stop();
		return state;
	}

	private TaskProviderStateManager getCleaned() {
		var state = getStopped();
		state.cleanup();
		return state;
	}
}