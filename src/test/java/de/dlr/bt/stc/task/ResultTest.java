package de.dlr.bt.stc.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.Test;

class ResultTest {

	@Test
	void ok() {
		Result ok = Result.ok("id");

		assertTrue(ok.isOk());
		assertFalse(ok.isNotOk());
		assertEquals("id", ok.getId());
		assertNull(ok.getErrorMsg());
		assertNull(ok.getException());
		assertEquals(ResultStatus.OK, ok.getStatus());
	}

	@Test
	void config_invalid() {
		var error = Result.configInvalid("id");

		assertTrue(error.isNotOk());
		assertFalse(error.isOk());
		assertEquals("id", error.getId());
		assertNull(error.getErrorMsg());
		assertNull(error.getException());
		assertEquals(ResultStatus.CONFIG_INVALID, error.getStatus());
	}

	@Test
	void config_invalid_with_exception() {
		var ex = new ConfigurationException("msg");
		var error = Result.configInvalid("id", ex);

		assertTrue(error.isNotOk());
		assertEquals("id", error.getId());
		assertEquals("msg", error.getErrorMsg());
		assertSame(ex, error.getException());
		assertEquals(ResultStatus.CONFIG_INVALID, error.getStatus());
	}

	@Test
	void canceled() {
		var error = Result.canceled("id");

		assertTrue(error.isNotOk());
		assertFalse(error.isOk());
		assertEquals("id", error.getId());
		assertNull(error.getErrorMsg());
		assertNull(error.getException());
		assertEquals(ResultStatus.CANCELED, error.getStatus());
	}

	@Test
	void error_with_exception() {
		var ex = new RuntimeException("a runtime exception");
		var error = Result.error("id", ex);

		assertTrue(error.isNotOk());
		assertEquals("id", error.getId());
		assertEquals("a runtime exception", error.getErrorMsg());
		assertSame(ex, error.getException());
		assertEquals(ResultStatus.ERROR, error.getStatus());
	}

	@Test
	void error_with_exception_and_message() {
		var ex = new RuntimeException("a runtime exception");
		var error = Result.error("id", ex, "msg");

		assertTrue(error.isNotOk());
		assertEquals("id", error.getId());
		assertEquals("msg", error.getErrorMsg());
		assertSame(ex, error.getException());
		assertEquals(ResultStatus.ERROR, error.getStatus());
	}

}
