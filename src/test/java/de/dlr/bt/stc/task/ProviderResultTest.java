package de.dlr.bt.stc.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ProviderResultTest {

	@Test
	void ok() {
		ProviderResult ok = new ProviderResult("id", ResultAction.INITIALIZE);

		ok.getTaskResults().add(Result.ok("id2"));

		assertTrue(ok.isOk());
		assertEquals("id", ok.getId());
		assertEquals("id2", ok.getTaskResults().getFirst().getId());
		assertEquals(ResultStatus.OK, ok.getTaskResults().getFirst().getStatus());
		assertEquals(ResultAction.INITIALIZE, ok.getAction());
	}

	@Test
	void error_with_exception() {
		ProviderResult error = new ProviderResult("id", ResultAction.INITIALIZE);

		error.getTaskResults().add(Result.error("id2", new RuntimeException("a runtime exc")));

		assertFalse(error.isOk());
		assertNotNull(error.getTaskResults().getFirst().getException());
		assertEquals("a runtime exc", error.getTaskResults().getFirst().getErrorMsg());
		assertEquals("id", error.getId());
		assertEquals(ResultStatus.ERROR, error.getTaskResults().getFirst().getStatus());
		assertEquals(ResultAction.INITIALIZE, error.getAction());
	}
}
