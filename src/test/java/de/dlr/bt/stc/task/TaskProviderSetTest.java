package de.dlr.bt.stc.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

class TaskProviderSetTest {

	@Test
	void testAddSource() {
		var sourceState = getSourceState();
		var set = new TaskProviderSet();
		set.addSource(sourceState);
		assertEquals(List.of(sourceState), set.getSources());
		assertEquals(Collections.emptyList(), set.getBridges());
		assertEquals(Collections.emptyList(), set.getSinks());
	}

	@Test
	void testAddBridge() {
		var bridgeState = getBridgeState();
		var set = new TaskProviderSet();
		set.addBridge(bridgeState);
		assertEquals(Collections.emptyList(), set.getSources());
		assertEquals(List.of(bridgeState), set.getBridges());
		assertEquals(Collections.emptyList(), set.getSinks());
	}

	@Test
	void testAddSink() {
		var sinkState = getSinkState();
		var set = new TaskProviderSet();
		set.addSink(sinkState);
		assertEquals(Collections.emptyList(), set.getSources());
		assertEquals(Collections.emptyList(), set.getBridges());
		assertEquals(List.of(sinkState), set.getSinks());
	}

	@Test
	void testAddProvider_source() {
		var sourceState = getSourceState();
		var set = new TaskProviderSet();
		set.addTaskProvider(sourceState);
		assertEquals(List.of(sourceState), set.getSources());
		assertEquals(Collections.emptyList(), set.getBridges());
		assertEquals(Collections.emptyList(), set.getSinks());
	}

	@Test
	void testAddProvider_bridge() {
		var bridgeState = getBridgeState();
		var set = new TaskProviderSet();
		set.addTaskProvider(bridgeState);
		assertEquals(Collections.emptyList(), set.getSources());
		assertEquals(List.of(bridgeState), set.getBridges());
		assertEquals(Collections.emptyList(), set.getSinks());
	}

	@Test
	void testAddProvider_sink() {
		var sinkState = getSinkState();
		var set = new TaskProviderSet();
		set.addTaskProvider(sinkState);
		assertEquals(Collections.emptyList(), set.getSources());
		assertEquals(Collections.emptyList(), set.getBridges());
		assertEquals(List.of(sinkState), set.getSinks());
	}

	@Test
	void testGetAllResults() {
		var sinkResults = List.of(new ProviderResult("ok1", ResultAction.INITIALIZE),
				new ProviderResult("ok2", ResultAction.INITIALIZE));
		var bridgeResults1 = List.of(new ProviderResult("ok3", ResultAction.START));
		var bridgeResults2 = List.of(new ProviderResult("ok4", ResultAction.STOP));
		var sourceResults = List.of(new ProviderResult("ok5", ResultAction.INITIALIZE));

		var sink = mock(TaskProviderStateManager.class);
		when(sink.getResults()).thenReturn(sinkResults);
		var bridge1 = mock(TaskProviderStateManager.class);
		when(bridge1.getResults()).thenReturn(bridgeResults1);
		var bridge2 = mock(TaskProviderStateManager.class);
		when(bridge2.getResults()).thenReturn(bridgeResults2);
		var source = mock(TaskProviderStateManager.class);
		when(source.getResults()).thenReturn(sourceResults);

		var set = new TaskProviderSet();
		set.addSink(sink);
		set.addBridge(bridge1);
		set.addBridge(bridge2);
		set.addSource(source);

		var actual = set.getAllResults();
		for (var results : List.of(sinkResults, bridgeResults1, bridgeResults2, sourceResults)) {
			for (var result : results) {
				assertTrue(actual.contains(result));
			}
		}
	}

	private TaskProviderStateManager getSourceState() {
		var source = mock(ISourceProvider.class);
		return new TaskProviderStateManager(source);
	}

	private TaskProviderStateManager getBridgeState() {
		var bridge = mock(IBridgeProvider.class);
		return new TaskProviderStateManager(bridge);
	}

	private TaskProviderStateManager getSinkState() {
		var sink = mock(ISinkProvider.class);
		return new TaskProviderStateManager(sink);
	}
}
