package de.dlr.bt.stc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.init.ModuleInitializer;
import de.dlr.bt.stc.source.AEndpointSourceCfg;
import de.dlr.bt.stc.source.opcua.SourceOPCUACfg;

class SourceOPCUACfgFileTest extends BaseTestCase {

	ConfigurationManager configuration;

	@BeforeAll
	public static void setupModules() {
		ModuleInitializer.initialize();
	}

	@Mock
	private EventBus instanceEventBus;
	@Mock
	private EventBus managementEventBus;

	@BeforeEach
	public void setupConfiguration() {
		var url = getClass().getClassLoader().getResource("config/sources.yml");
		configuration = new ConfigurationManager(instanceEventBus, managementEventBus);
		try {
			Path p = Paths.get(url.toURI());
			this.configuration.addConfigurationPath(p);
			this.configuration.loadConfigurations();

		} catch (URISyntaxException e) {
			fail(e);
		}
	}

	@Test
	void testNumberOfSources() {
		assertEquals(4, getSourceConfigurations(configuration).size());
	}

	@Test
	void testInheritance() {
		var opcuachild1 = (SourceOPCUACfg) getSourceConfigurations(configuration).get("opcua_child1");
		var opcuachild2 = (SourceOPCUACfg) getSourceConfigurations(configuration).get("opcua_child2");

		assertEquals(2, opcuachild1.getQueueSize()); // directly defined
		assertEquals(1, opcuachild2.getQueueSize()); // inherited from template
	}

	@Test
	void testNonExistingProperty() {
		var opcuachild2 = (SourceOPCUACfg) getSourceConfigurations(configuration).get("opcua_child2");

		assertNull(opcuachild2.getNodeId());
	}

	private static Map<String, AEndpointSourceCfg> getSourceConfigurations(ConfigurationManager cfg) {
		Map<String, AEndpointSourceCfg> result = new HashMap<>();
		for (var entry : cfg.getConfigurations().entrySet())
			if (entry.getValue() instanceof AEndpointSourceCfg asc)
				result.put(entry.getKey(), asc);
		return result;
	}
}
