package de.dlr.bt.stc.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.time.Instant;

import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.init.ModuleInitializer;
import de.dlr.bt.stc.opcuaserver.UAServer;
import de.dlr.bt.stc.source.opcua.MiloClient;
import de.dlr.bt.stc.source.opcua.MiloClientCfg;
import de.dlr.bt.stc.source.opcua.MiloSource;
import de.dlr.bt.stc.source.opcua.SubscriptionParameters;

class MiloClientTest extends BaseTestCase {
	@Mock
	MiloClientCfg cfg;

	// Mocked
	static EventBus eventBus;

	// Not mocked
	static UAServer uaserver;

	@BeforeAll
	static void startupServer() throws Exception {
		ModuleInitializer.initialize();
		eventBus = mock(EventBus.class);
		uaserver = new UAServer(12345, eventBus);
		uaserver.startup().get();
	}

	@AfterAll
	static void shutdownServer() throws Exception {
		uaserver.shutdown().get();
	}

	MiloClient mc;

	@BeforeEach
	void setupConfig() throws Exception {
		when(cfg.getEndpoint()).thenReturn("opc.tcp://localhost:12345/");
		when(cfg.getSecurityPolicy()).thenReturn(SecurityPolicy.None);
		mc = new MiloClient(cfg);
	}

	@AfterEach
	void closeClient() {
		mc.cleanup();
	}

	@Test
	void testReadServerTime() throws Exception {

		// Read current time from server
		var nv = mc.readNodeValue(Identifiers.Server_ServerStatus_CurrentTime);
		assertTrue(nv.getStatusCode().isGood());
		var val = nv.getValue().getValue();
		if (val instanceof DateTime valtime) {
			var duration = Duration.between(valtime.getJavaInstant(), Instant.now());
			// Server time should not be longer ago than 1000 ms
			assertTrue(duration.abs().toMillis() < 1000);
		} else {
			fail("No DateTime returned!");
		}
	}

	@Test
	void testSubscribeServerTime() throws Exception {
		MiloSource ms = mock(MiloSource.class);

		SubscriptionParameters sp = new SubscriptionParameters(100, 100, 10);

		mc.registerNodeSubscription(ms, sp, Identifiers.Server_ServerStatus_CurrentTime);
		mc.updateSubscriptions();

		var mic = ArgumentCaptor.forClass(UaMonitoredItem.class);

		verify(ms, timeout(1000)).onSubscriptionValue(mic.capture(), any());
		assertEquals(Identifiers.Server_ServerStatus_CurrentTime, mic.getValue().getReadValueId().getNodeId());

		mc.removeSubscriptions();
	}

	@Test
	void testResolveNodePath() throws Exception {
		var nodeitems = mc.resolveNodePath("Objects/Server/ServerStatus/{{ .* | variable}}");

		assertEquals(6, nodeitems.size());
		var nodeids = nodeitems.keySet();
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_BuildInfo));
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_CurrentTime));
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_SecondsTillShutdown));
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_ShutdownReason));
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_StartTime));
		assertTrue(nodeids.contains(Identifiers.Server_ServerStatus_State));

		var currentTime = nodeitems.get(Identifiers.Server_ServerStatus_CurrentTime);
		assertEquals("CurrentTime", currentTime.getVarMap().get("variable"));
	}
}
