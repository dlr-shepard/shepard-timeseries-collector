package de.dlr.bt.stc.source.mqtt;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class MqttClientWrapperTest {
	private MqttClientWrapper sut;

	private final IMqttClient client = Mockito.mock(IMqttClient.class);
	private final MqttConnectOptions options = Mockito.mock(MqttConnectOptions.class);

	private static final String TOPIC = "TOPIC/+";

	private static final String SOURCE_KEY = "SOURCE_KEY_1";

	private static final int QUALITY_OF_SERVICE = 0;

	@BeforeEach
	public void init() throws SourceConfigurationException, MqttException {
		sut = new MqttClientWrapper(client, options);
		var subscriber = Mockito.mock(MqttSubscriber.class);
		var mqttTopic = Mockito.mock(MqttTopic.class);
		when(mqttTopic.getActualTopic()).thenReturn(TOPIC);
		when(subscriber.getMqttTopic()).thenReturn(mqttTopic);
		sut.subscribe(TOPIC, QUALITY_OF_SERVICE, subscriber);
	}

	@Test
	void connectComplete_reload() throws MqttException {
		sut.connectComplete(true, SOURCE_KEY);

		// Twice, because initial subscription is also counted
		verify(client, times(2)).subscribe(TOPIC, QUALITY_OF_SERVICE, sut);
		verify(client).setCallback(sut);
	}

	@Test
	void connectComplete_no_reload() throws MqttException {
		sut.connectComplete(false, SOURCE_KEY);

		// Once, because initial subscription is also counted
		verify(client, times(1)).subscribe(TOPIC, QUALITY_OF_SERVICE, sut);
	}

	@Test
	void no_exception_on_log_methods() throws MqttException {
		sut.connectionLost(null);
		sut.deliveryComplete(null);
		assertTrue(true);
	}

	@Test
	void close() throws MqttException {
		sut.close();

		verify(client).disconnect();
		verify(client).close();
	}
}
