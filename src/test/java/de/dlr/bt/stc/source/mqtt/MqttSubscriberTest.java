package de.dlr.bt.stc.source.mqtt;

import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MqttSubscriberTest {

	private static String TOPIC = "TOPIC/+";

	private static final String SOURCE_KEY = "SOURCE_KEY_1";

	private static final int QUALITY_OF_SERVICE = 0;

	private final MqttClientWrapper clientWrapper = Mockito.mock(MqttClientWrapper.class);

	private final EventBus eventBus = Mockito.mock(EventBus.class);

	private MqttSubscriber sut;

	@BeforeEach
	public void init() throws SourceConfigurationException {
		sut = MqttSubscriber.builder()
				.clientWrapper(clientWrapper)
				.eventBus(eventBus)
				.qos(QUALITY_OF_SERVICE)
				.sourceKey(SOURCE_KEY)
				.topic("TOPIC/{{ [0-9] | variable }}")
				.build();
	}

	@Test
	void messageArrived() throws MqttException, STCException {
		checkMessageArrived(sut, "TOPIC/1", "a String".getBytes());
		checkMessageArrived(sut, "TOPIC/2", "{ text: 'A JSON String'}".getBytes());

		byte[] bytesOfInteger = ByteBuffer.allocate(4).putInt(123).array();
		checkMessageArrived(sut, "TOPIC/3", bytesOfInteger);

		checkMessageArrived_invalid_topic(sut, "TOPIC/abc", "a String".getBytes());
	}

	private void checkMessageArrived(MqttSubscriber subscriber, String topic, byte[] payload)
			throws MqttException, STCException {
		subscriber.messageArrived(topic, createMessage(payload));

		var captor = ArgumentCaptor.forClass(DataAvailableEvent.class);

		verify(eventBus).post(captor.capture());
		reset(eventBus);

		checkEvent(payload, captor.getValue());
	}

	private void checkMessageArrived_invalid_topic(MqttSubscriber subscriber, String topic, byte[] payload)
			throws MqttException, STCException {
		subscriber.messageArrived(topic, createMessage(payload));

		var captor = ArgumentCaptor.forClass(DataAvailableEvent.class);

		verify(eventBus, never()).post(captor.capture());
		reset(eventBus);
	}

	private void checkEvent(byte[] payload, DataAvailableEvent event) {
		assertEquals(new String(payload), event.getValue());
		assertNotEquals(0L, event.getTimestamp());
		assertNotNull(event.getVariableMap());
		assertEquals(SOURCE_KEY, event.getSourceKey());
	}

	private MqttMessage createMessage(byte[] payload) {
		var message = new MqttMessage();
		message.setId(10);
		message.setQos(QUALITY_OF_SERVICE);
		message.setPayload(payload);
		return message;
	}
}
