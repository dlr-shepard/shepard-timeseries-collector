package de.dlr.bt.stc.source.mqtt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.CredentialsCfg;

class MqttFactoryTest {

	private static final String ENDPOINT = "tcp://localhost:1234567";

	private static final String TOPIC = "TOPIC";

	private static final String SOURCE_KEY = "SOURCE_KEY_1";

	private static final String USERNAME = "user";

	private static final String PASSWORD = "password";

	private static final int QUALITY_OF_SERVICE = 0;

	private static final double CONNECTION_TIMEOUT = 10.0;

	private final MqttFactory sut = MqttFactory.getInstance();

	@Test
	public void createMqttSubscriber() throws SourceConfigurationException {
		var eventBus = Mockito.mock(EventBus.class);
		var client = Mockito.mock(MqttClientWrapper.class);
		var config = mockConfig();

		var subscriber = sut.createMqttSubscriber(client, config, eventBus);

		assertEquals(TOPIC, subscriber.getMqttTopic().getActualTopic());
		assertEquals(QUALITY_OF_SERVICE, subscriber.getQos());
		assertEquals(SOURCE_KEY, subscriber.getSourceKey());
		assertEquals(eventBus, subscriber.getEventBus());
		assertEquals(client, subscriber.getClientWrapper());
	}

	@Test
	public void createMqttClient() throws MqttException {
		var options = Mockito.mock(MqttConnectOptions.class);

		var client = sut.createMqttClient("subscriberId", ENDPOINT, options);

		assertNotNull(client.getClient().getClientId());
		assertEquals(ENDPOINT, client.getClient().getServerURI());
	}

	@Test
	public void createMqttClientOptions() {
		var config = mockCLientConfig();

		var options = sut.createMqttClientOptions(config);

		assertTrue(options.isAutomaticReconnect());
		assertFalse(options.isCleanSession());
		assertEquals(CONNECTION_TIMEOUT, options.getConnectionTimeout());
		assertEquals(USERNAME, options.getUserName());
		assertEquals(PASSWORD, new String(options.getPassword()));
	}

	@Test
	public void createMqttClientOptions_no_credentials() {
		var config = mockCLientConfig();
		when(config.getCredentials()).thenReturn(null);

		var options = sut.createMqttClientOptions(config);

		assertTrue(options.isAutomaticReconnect());
		assertFalse(options.isCleanSession());
		assertEquals(CONNECTION_TIMEOUT, options.getConnectionTimeout());
		assertNull(options.getUserName());
		assertNull(options.getPassword());
	}

	private SourceMQTTCfg mockConfig() {
		SourceMQTTCfg config = Mockito.mock(SourceMQTTCfg.class);
		CredentialsCfg credentials = Mockito.mock(CredentialsCfg.class);
		when(config.getTopic()).thenReturn(TOPIC);
		when(config.getId()).thenReturn(SOURCE_KEY);
		when(config.getQos()).thenReturn(QUALITY_OF_SERVICE);
		when(config.getEndpoint()).thenReturn(ENDPOINT);
		when(config.getCredentials()).thenReturn(credentials);
		when(config.getConnectTimeout()).thenReturn(CONNECTION_TIMEOUT);
		when(credentials.getUsername()).thenReturn(USERNAME);
		when(credentials.getPassword()).thenReturn(PASSWORD);
		return config;
	}

	private MqttClientCfg mockCLientConfig() {
		MqttClientCfg config = Mockito.mock(MqttClientCfg.class);
		CredentialsCfg credentials = Mockito.mock(CredentialsCfg.class);
		when(config.getEndpoint()).thenReturn(ENDPOINT);
		when(config.getConnectTimeout()).thenReturn(CONNECTION_TIMEOUT);
		when(config.getCredentials()).thenReturn(credentials);
		when(credentials.getUsername()).thenReturn(USERNAME);
		when(credentials.getPassword()).thenReturn(PASSWORD);
		return config;
	}
}
