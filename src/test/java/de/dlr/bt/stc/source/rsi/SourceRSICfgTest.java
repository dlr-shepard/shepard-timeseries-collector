package de.dlr.bt.stc.source.rsi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.source.SourceDataType;

class SourceRSICfgTest extends BaseTestCase {
	@Mock
	HierarchicalConfiguration<?> config;

	SourceRSICfg cfg;

	@BeforeEach
	void setup() {
		cfg = new SourceRSICfg(config);
	}

	@Test
	void testPort() {
		when(config.getInt("port", 0)).thenReturn(12345);
		assertEquals(12345, cfg.getPort());
	}

	@Test
	void testPath() {
		when(config.getString("path", "/")).thenReturn("/1234/");
		assertEquals("/1234/", cfg.getPath());
	}

	@Test
	void testDatatype() {
		when(config.getString(eq("datatype"), any())).thenReturn("String");
		assertEquals(SourceDataType.STRING, cfg.getDatatype());
		when(config.getString(eq("datatype"), any())).thenReturn("bool");
		assertEquals(SourceDataType.BOOL, cfg.getDatatype());
		when(config.getString(eq("datatype"), any())).thenReturn("float");
		assertEquals(SourceDataType.FLOAT, cfg.getDatatype());
		when(config.getString(eq("datatype"), any())).thenReturn("int");
		assertEquals(SourceDataType.INTEGER, cfg.getDatatype());

	}

}
