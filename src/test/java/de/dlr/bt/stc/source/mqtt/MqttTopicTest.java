package de.dlr.bt.stc.source.mqtt;

import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class MqttTopicTest {

	@Test
	void checkTopic() throws SourceConfigurationException {
		var topic = "test/{{ ac_[0-9](-[a-z])? | name }}/numbers/{{ [0-9]+ | numbers }}";
		var mqttTopic = new MqttTopic(topic);
		assertEquals(topic, mqttTopic.getTopic());
	}

	@Test
	void checkActualTopic_no_closing_brackets() throws SourceConfigurationException {
		var topic = "test/{{";
		var mqttTopic = new MqttTopic(topic);
		assertEquals(topic, mqttTopic.getActualTopic());
	}

	@Test
	void checkActualTopic() throws SourceConfigurationException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}/numbers/{{ [0-9]+ | numbers }}");
		assertEquals("test/+/numbers/+", mqttTopic.getActualTopic());
	}

	@ParameterizedTest
	@ValueSource(strings = {"test/ac_1", "test/ac_2", "test/ac_1-a"})
	void checkTopicMatching_match(String topic) throws SourceConfigurationException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}");
		assertTrue(mqttTopic.checkTopicMatching(topic));
	}

	@Test
	void checkTopicMatching_no_template() throws SourceConfigurationException {
		var mqttTopic = new MqttTopic("test/ac_1");
		assertTrue(mqttTopic.checkTopicMatching("test/ac_1"));
	}

	@ParameterizedTest
	@ValueSource(strings = {"test/xyz", "abc123", "//"})
	void checkTopicMatching_no_match(String topic) throws SourceConfigurationException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}");
		assertFalse(mqttTopic.checkTopicMatching(topic));
	}

	@ParameterizedTest
	@ValueSource(strings = {"test/{{ [ | test }}", "test/{{ abc | test1 | test2 }}", "test/{{ abc }}"})
	void checkConstruction_invalidTemplate(String topic) throws SourceConfigurationException {
		assertThrows(SourceConfigurationException.class, () -> new MqttTopic(topic));
	}

	@Test
	void checkExtractVariables() throws SourceConfigurationException, STCException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}/numbers/{{ [0-9]+ | numbers }}");
		var actual = mqttTopic.extractVariables("test/ac_1-a/numbers/123");
		var expected = Map.of("name", "ac_1-a", "numbers", "123");
		assertEquals(expected, actual);
	}

	@Test
	void checkExtractVariables_no_templates() throws SourceConfigurationException, STCException {
		var mqttTopic = new MqttTopic("test/123");
		var actual = mqttTopic.extractVariables("test/123");
		var expected = Collections.emptyMap();
		assertEquals(expected, actual);
	}

	@Test
	void checkExtractVariables_invalid_topic() throws SourceConfigurationException, STCException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}/numbers/{{ [0-9]+ | numbers }}");
		var ex = assertThrows(STCException.class, () -> mqttTopic.extractVariables("test/ac_1-a"));
		assertEquals("Unexpected topic: test/ac_1-a", ex.getMessage());
	}

	@Test
	void checkExtractVariables_invalid_topic_elements() throws SourceConfigurationException, STCException {
		var mqttTopic = new MqttTopic("test/{{ ac_[0-9](-[a-z])? | name }}");
		var ex = assertThrows(STCException.class, () -> mqttTopic.extractVariables("test/invalid"));
		assertEquals("Topic element does not match: test/invalid", ex.getMessage());
	}
}
