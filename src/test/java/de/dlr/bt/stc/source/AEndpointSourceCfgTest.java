package de.dlr.bt.stc.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationRuntimeException;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

class AEndpointSourceCfgTest extends BaseTestCase {
	@Mock
	private HierarchicalConfiguration<?> config;

	private class TestSourceCfg extends AEndpointSourceCfg {

		protected TestSourceCfg(HierarchicalConfiguration<?> config) {
			super(config);
		}

	}

	@Test
	void testGetEndpoint() {
		when(config.getString("endpoint", "")).thenReturn("Endpoint");
		var cfg = new TestSourceCfg(config);
		assertEquals("Endpoint", cfg.getEndpoint());
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	void testGetCredentials() {
		HierarchicalConfiguration credentials = mock(HierarchicalConfiguration.class);
		when(credentials.getString("username")).thenReturn("username");
		when(credentials.getString("password")).thenReturn("password");

		when(config.configurationAt("credentials")).thenReturn(credentials);
		var cfg = new TestSourceCfg(config);
		assertEquals("username", cfg.getCredentials().getUsername());

		when(config.configurationAt("credentials")).thenThrow(ConfigurationRuntimeException.class);
		assertNull(cfg.getCredentials());
	}

}
