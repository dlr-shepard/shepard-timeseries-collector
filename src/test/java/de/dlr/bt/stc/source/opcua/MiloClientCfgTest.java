package de.dlr.bt.stc.source.opcua;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationRuntimeException;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

class MiloClientCfgTest extends BaseTestCase {
	@Mock
	HierarchicalConfiguration<?> config;

	MiloClientCfg cfg;

	@BeforeEach
	void setup() {
		cfg = new MiloClientCfg(new SourceOPCUACfg(config));
	}

	@Test
	void getSecurityPolicy() {
		when(config.getString(eq("security_policy"), any())).thenReturn("None");
		assertEquals(SecurityPolicy.None, cfg.getSecurityPolicy());
		when(config.getString(eq("security_policy"), any())).thenReturn("Basic256Sha256");
		assertEquals(SecurityPolicy.Basic256Sha256, cfg.getSecurityPolicy());
	}

	@Test
	void getKeystorePath() {
		when(config.getString(eq("keystore_path"), any())).thenReturn("keystore");
		assertEquals("keystore", cfg.getKeystorePath());
	}

	@Test
	void getKeystorePassword() {
		when(config.getString(eq("keystore_password"), any())).thenReturn("password");
		assertEquals("password", cfg.getKeystorePassword());
	}

	@Test
	void isCreateKeystore() {
		when(config.getBoolean("keystore_create", true)).thenReturn(true);
		assertEquals(true, cfg.isCreateKeystore());
	}

	@Test
	void testGetEndpoint() {
		when(config.getString("endpoint", "")).thenReturn("Endpoint");
		assertEquals("Endpoint", cfg.getEndpoint());
	}

	@Test
	void testGetEndpointLC() {
		when(config.getString("endpoint", "")).thenReturn("Endpoint");
		assertEquals("endpoint", cfg.getEndpointLC());
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	void testGetCredentials() {
		HierarchicalConfiguration credentials = mock(HierarchicalConfiguration.class);
		when(credentials.getString("username")).thenReturn("username");
		when(credentials.getString("password")).thenReturn("password");

		when(config.configurationAt("credentials")).thenReturn(credentials);
		assertEquals("username", cfg.getCredentials().getUsername());

		when(config.configurationAt("credentials")).thenThrow(ConfigurationRuntimeException.class);
		assertNull(cfg.getCredentials());
	}

}
