package de.dlr.bt.stc.source.mqtt;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.task.Result;
import de.dlr.bt.stc.task.ResultStatus;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class MqttSourceTest {

	@Test
	void initializeTask() throws Exception {
		var factory = mock(MqttFactory.class);
		var eventBus = mock(EventBus.class);
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		mockConfig(config);
		mockFactory(factory, eventBus, client, subscriber, config);

		var sut = MqttSource.builder()
				.instanceEventBus(eventBus)
				.mqttFactory(factory)
				.taskId("taskId")
				.sourceConfig(config)
				.mqttClient(client)
				.build();

		sut.initializeTask();

		verify(factory).createMqttSubscriber(any(), eq(config), eq(eventBus));
		verify(client).connect();
	}

	@Test
	void initializeTask_config_invalid() throws Exception {
		var factory = mock(MqttFactory.class);
		var eventBus = mock(EventBus.class);
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		mockConfig(config);
		mockFactory(factory, eventBus, client, subscriber, config);
		when(config.getEndpoint()).thenReturn(null);

		var sut = MqttSource.builder()
				.instanceEventBus(eventBus)
				.mqttFactory(factory)
				.taskId("taskId")
				.sourceConfig(config)
				.mqttClient(client)
				.build();

		Result result = sut.initializeTask();

		assertTrue(result.isNotOk());
		assertEquals("MQTT Source taskId: Problems creating MQTT Source ", result.getErrorMsg());
		assertEquals(ResultStatus.CONFIG_INVALID, result.getStatus());
		assertInstanceOf(SourceConfigurationException.class, result.getException());
	}

	@Test
	void initializeTask_mqtt_exception() throws Exception {
		var factory = mock(MqttFactory.class);
		var eventBus = mock(EventBus.class);
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		mockConfig(config);
		mockFactory(factory, eventBus, client, subscriber, config);

		var sut = MqttSource.builder()
				.instanceEventBus(eventBus)
				.mqttFactory(factory)
				.taskId("taskId")
				.sourceConfig(config)
				.mqttClient(client)
				.build();

		doThrow(MqttException.class).when(client).connect();

		Result result = sut.initializeTask();

		assertTrue(result.isNotOk());
		assertEquals("MQTT Source taskId: Problems creating MQTT Subscriber ", result.getErrorMsg());
		assertEquals(ResultStatus.ERROR, result.getStatus());
		assertInstanceOf(SourceConfigurationException.class, result.getException());
	}

	@Test
	void startTask() throws Exception {
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		mockConfig(config);

		var sut = MqttSource.builder()
				.taskId("taskId")
				.mqttClient(client)
				.mqttSubscriber(subscriber)
				.sourceConfig(config)
				.build();

		sut.startTask();

		verify(subscriber).start();
	}

	@Test
	void startTask_client_exception() throws Exception {
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		doThrow(new MqttException(null)).when(subscriber).start();

		mockConfig(config);

		var sut = MqttSource.builder()
				.taskId("taskId")
				.mqttClient(client)
				.mqttSubscriber(subscriber)
				.sourceConfig(config)
				.build();

		Result result = sut.startTask();
		assertTrue(result.isNotOk());
		assertInstanceOf(MqttException.class, result.getException());
		assertEquals("Problems connecting to MQTT TOPIC : topic", result.getErrorMsg());
	}

	@Test
	void stopTask() throws SourceConfigurationException, MqttException {
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);

		var sut = MqttSource.builder()
				.taskId("taskId")
				.mqttClient(client)
				.mqttSubscriber(subscriber)
				.build();

		sut.stopTask();

		verify(client).close();

		verify(client).close();
	}

	@Test
	void stopTask_client_exception() throws Exception {
		var client = mock(MqttClientWrapper.class);
		var subscriber = mock(MqttSubscriber.class);
		var config = mock(SourceMQTTCfg.class);

		doThrow(new MqttException(null)).when(client).close();

		mockConfig(config);

		var sut = MqttSource.builder()
				.taskId("taskId")
				.mqttClient(client)
				.mqttSubscriber(subscriber)
				.sourceConfig(config)
				.build();

		Result result = sut.stopTask();
		assertTrue(result.isNotOk());
		assertInstanceOf(MqttException.class, result.getException());
		assertEquals("MQTT Source taskId: Problems disconnecting MQTT Subscriber", result.getErrorMsg());
	}

	private void mockFactory(MqttFactory factory, EventBus eventBus, MqttClientWrapper client,
							 MqttSubscriber subscriber, SourceMQTTCfg config) throws MqttException, SourceConfigurationException {
		when(factory.createMqttClient(any(), any(), any())).thenReturn(client);
		when(factory.createMqttSubscriber(client, config, eventBus)).thenReturn(subscriber);
	}

	private void mockConfig(SourceMQTTCfg config) {
		when(config.getEndpoint()).thenReturn("mqtt://example.com:1883");
		when(config.getTopic()).thenReturn("topic");
		when(config.getQos()).thenReturn(0);
		when(config.getId()).thenReturn("taskId");
	}
}
