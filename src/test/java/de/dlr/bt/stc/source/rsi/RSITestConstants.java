package de.dlr.bt.stc.source.rsi;

public class RSITestConstants {
	public static final String TESTMESSAGE = """
			 <Rob TYPE="KUKA">
			    <RIst X="0.0" Y="1.0" Z="2.0" A="3.0" B="4.0" C="5.5" />
			    <AIPos A1="0.0" A2="0.0" A3="0.0" A4="0.0" A5="0.0" A6="0.0" />
			    <IPOC>123645634563</IPOC>
			    <Mode>1</Mode>
			    <Offset>0</Offset>
			</Rob>""";

}
