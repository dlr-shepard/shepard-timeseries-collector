package de.dlr.bt.stc.sink.shepard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.shepard.client.java.auth.ApiKeyAuth;

public class ShepardApiFactoryTest extends BaseTestCase {

	private ShepardApiFactory factory = new ShepardApiFactory();

	@Test
	void getTimeseriesApiTest() {
		var actual = factory.getTimeseriesApi("http://host", "123abc");
		var auth = (ApiKeyAuth) actual.getApiClient().getAuthentication("apikey");
		assertEquals("http://host", actual.getApiClient().getBasePath());
		assertEquals("123abc", auth.getApiKey());
	}
}
