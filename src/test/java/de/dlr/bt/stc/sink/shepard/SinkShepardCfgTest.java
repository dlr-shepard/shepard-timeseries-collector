package de.dlr.bt.stc.sink.shepard;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import de.dlr.bt.stc.BaseTestCase;

public class SinkShepardCfgTest extends BaseTestCase {

	@Mock
	private HierarchicalConfiguration<?> config;

	@Test
	void testGetHost() {
		when(config.getString("host")).thenReturn("http://host");
		var cfg = new SinkShepardCfg(config);
		assertEquals("http://host", cfg.getHost());
	}

	@Test
	void testGetApiKey() {
		when(config.getString("api_key")).thenReturn("123abc");
		var cfg = new SinkShepardCfg(config);
		assertEquals("123abc", cfg.getApiKey());
	}

	@Test
	void testGetTimeseriesContainerId() {
		when(config.getLong("timeseries_container_id")).thenReturn(123L);
		var cfg = new SinkShepardCfg(config);
		assertEquals(123L, cfg.getTimeseriesContainerId());
	}

}
