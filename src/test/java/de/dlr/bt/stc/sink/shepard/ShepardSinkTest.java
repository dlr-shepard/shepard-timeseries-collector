package de.dlr.bt.stc.sink.shepard;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import de.dlr.bt.stc.BaseTestCase;
import de.dlr.bt.stc.entities.DataPoint;
import de.dlr.bt.stc.entities.Mapping;
import de.dlr.bt.stc.eventbus.CacheFullEvent;
import de.dlr.shepard.client.java.ApiException;
import de.dlr.shepard.client.java.api.TimeseriesContainerApi;
import de.dlr.shepard.client.java.model.InfluxPoint;
import de.dlr.shepard.client.java.model.Timeseries;
import de.dlr.shepard.client.java.model.TimeseriesPayload;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.greenrobot.eventbus.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

class ShepardSinkTest extends BaseTestCase {

	@Mock
	private EventBus eventBus;

	@Mock
	private SinkShepardCfg cfg;

	@Mock
	private TimeseriesContainerApi api;

	@Mock
	private ShepardApiFactory shepardApiFactory;

	@Mock
	private ThreadPoolExecutor executor;

	private ShepardSink shepardSink;

	@Captor
	private ArgumentCaptor<TimeseriesPayload> timeseriesPayloadCaptor;

	@BeforeEach
	void initCfg() throws ConfigurationException, NoSuchFieldException, SecurityException, IllegalArgumentException,
						  IllegalAccessException {
		when(cfg.getTimeseriesContainerId()).thenReturn(123L);
		when(cfg.getHost()).thenReturn("http://test.com");
		when(cfg.getApiKey()).thenReturn("apiKey123");
		when(shepardApiFactory.getTimeseriesApi("http://test.com", "apiKey123")).thenReturn(api);
		shepardSink = new ShepardSink("key", cfg, eventBus);

		var apiFactoryField = ShepardSink.class.getDeclaredField("shepardApiFactory");
		apiFactoryField.setAccessible(true);
		apiFactoryField.set(shepardSink, shepardApiFactory);

		// Overwrite the executor to directly execute the given runnable
		var executorField = ShepardSink.class.getDeclaredField("executor");
		executorField.setAccessible(true);
		executorField.set(shepardSink, executor);
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				final Runnable runnable = (Runnable) (invocation.getArguments())[0];
				runnable.run();
				return null;
			}
		}).when(executor).execute(any(Runnable.class));

		shepardSink.initializeTask();
	}

	@Test
	void stopTaskTest() {
		shepardSink.stopTask();
		verify(executor).shutdown();
	}

	@Test
	void joinTaskTest() throws InterruptedException {
		shepardSink.joinTask();
		verify(executor).awaitTermination(5, TimeUnit.SECONDS);
	}

	@Test
	void validate_valid() {
		when(cfg.getTimeseriesContainerId()).thenReturn(123L);
		when(cfg.getHost()).thenReturn("http://test.com");
		when(cfg.getApiKey()).thenReturn("apiKey123");
		shepardSink = new ShepardSink("key", cfg, eventBus);

		assertTrue(shepardSink.validate());
	}

	@ParameterizedTest
	@CsvSource({"-123, https://example.com, apiKey123", "123, invalid, apiKey123", "123, https://example.com, ''"})
	void validate_invalidHost(long containerId, String host, String apiKey) {
		when(cfg.getTimeseriesContainerId()).thenReturn(containerId);
		when(cfg.getHost()).thenReturn(host);
		when(cfg.getApiKey()).thenReturn(apiKey);
		shepardSink = new ShepardSink("key", cfg, eventBus);

		assertFalse(shepardSink.validate());
	}

	@Test
	void eventTest_notInitialized() throws ApiException {
		var data = List.of(new DataPoint(1, null), new DataPoint(2, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		var shepardSink = new ShepardSink("key", cfg, eventBus);

		shepardSink.handleEvent(event);
		verify(api, never()).createTimeseries(any(Long.class), any(TimeseriesPayload.class));
	}

	private static Stream<Arguments> eventTest() {
		// @formatter:off
	    return Stream.of(
	    		Arguments.of(5, 5),
	    	    Arguments.of(5.5, 5.5),
	    	    Arguments.of(5.0, 5.0),
	    	    Arguments.of(true, true),
	    	    Arguments.of("string", "string"),
	    	    Arguments.of("123", "123"),
	    	    Arguments.of("12.3", "12.3"),
	    	    Arguments.of("true", "true"),
	    	    Arguments.of("false", "false"),
	    	    Arguments.of(new DataPoint(1, 2), "DataPoint(timestamp=1, value=2)")
	    	    );
		// @formatter:on
	}

	@ParameterizedTest
	@MethodSource
	void eventTest(Object inputValue, Object expectedValue) throws ApiException {
		var data = List.of(new DataPoint(1, inputValue));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));

		shepardSink.handleEvent(event);

		var timeseriesExpected = new Timeseries();
		timeseriesExpected.setMeasurement("meas");
		timeseriesExpected.setLocation("loc");
		timeseriesExpected.setDevice("dev");
		timeseriesExpected.setSymbolicName("symName");
		timeseriesExpected.setField("field");
		var pointExpected = new InfluxPoint();
		pointExpected.setTimestamp(1l);
		pointExpected.setValue(expectedValue);
		var expected = new TimeseriesPayload();
		expected.setTimeseries(timeseriesExpected);
		expected.setPoints(List.of(pointExpected));
		verify(api).createTimeseries(eq(123L), timeseriesPayloadCaptor.capture());
		assertEquals(expected, timeseriesPayloadCaptor.getValue());
	}

	@Test
	void eventTest_noFieldProvided() throws ApiException {
		var data = List.of(new DataPoint(1, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", null));
		when(cfg.getField()).thenReturn("");

		shepardSink.handleEvent(event);

		var timeseriesExpected = new Timeseries();
		timeseriesExpected.setMeasurement("meas");
		timeseriesExpected.setLocation("loc");
		timeseriesExpected.setDevice("dev");
		timeseriesExpected.setSymbolicName("symName");
		timeseriesExpected.setField(null);
		var pointExpected = new InfluxPoint();
		pointExpected.setTimestamp(1l);
		pointExpected.setValue(2);
		var expected = new TimeseriesPayload();
		expected.setTimeseries(timeseriesExpected);
		expected.setPoints(List.of(pointExpected));
		verify(api).createTimeseries(eq(123L), timeseriesPayloadCaptor.capture());
		assertEquals(expected, timeseriesPayloadCaptor.getValue());
	}

	@Test
	void eventTest_valueIsNull() throws ApiException {
		var data = List.of(new DataPoint(1, null), new DataPoint(2, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));

		shepardSink.handleEvent(event);

		var timeseriesExpected = new Timeseries();
		timeseriesExpected.setMeasurement("meas");
		timeseriesExpected.setLocation("loc");
		timeseriesExpected.setDevice("dev");
		timeseriesExpected.setSymbolicName("symName");
		timeseriesExpected.setField("field");
		var pointExpected = new InfluxPoint();
		pointExpected.setTimestamp(2l);
		pointExpected.setValue(2);
		var expected = new TimeseriesPayload();
		expected.setTimeseries(timeseriesExpected);
		expected.setPoints(List.of(pointExpected));
		verify(api).createTimeseries(eq(123L), timeseriesPayloadCaptor.capture());
		assertEquals(expected, timeseriesPayloadCaptor.getValue());
	}

	@Test
	void eventTest_noPoints() throws ApiException {
		var data = List.of(new DataPoint(1, null), new DataPoint(2, null));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));

		shepardSink.handleEvent(event);

		verify(api, never()).createTimeseries(any(), any());
	}

	@Test
	void eventTest_ApiError() throws ApiException {
		var data = List.of(new DataPoint(1, 2));
		var event = new CacheFullEvent("key", data, new Mapping("meas", "loc", "dev", "symName", "field"));
		doThrow(new ApiException()).when(api).createTimeseries(any(Long.class), any(TimeseriesPayload.class));

		assertDoesNotThrow(() -> shepardSink.handleEvent(event));
	}

	@Test
	void getExecutorActiveCountTest() {
		when(executor.getActiveCount()).thenReturn(1);

		var diag = shepardSink.getExecutorActiveCount();
		assertEquals(1, diag);
	}

	@Test
	void getExecutorPoolSizeTest() {
		when(executor.getPoolSize()).thenReturn(1);

		var diag = shepardSink.getExecutorPoolSize();
		assertEquals(1, diag);
	}

	@Test
	void getExecutorLargestPoolSizeTest() {
		when(executor.getLargestPoolSize()).thenReturn(1);

		var diag = shepardSink.getExecutorLargestPoolSize();
		assertEquals(1, diag);
	}
}