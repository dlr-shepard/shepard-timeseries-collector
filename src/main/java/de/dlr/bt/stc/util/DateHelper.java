package de.dlr.bt.stc.util;

import java.util.Date;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DateHelper {

	public Date getDate() {
		return new Date();
	}

}
