package de.dlr.bt.stc.config;

import java.io.InputStream;

import org.apache.commons.configuration2.YAMLConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.yaml.snakeyaml.LoaderOptions;

public class YAMLConfigurationSTC extends YAMLConfiguration {
	@Override
	public void read(InputStream in) throws ConfigurationException {
		LoaderOptions lo = new LoaderOptions();
		lo.setAllowDuplicateKeys(false);
		super.read(in, lo);
	}
}
