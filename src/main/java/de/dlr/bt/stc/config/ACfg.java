package de.dlr.bt.stc.config;

import org.apache.commons.configuration2.HierarchicalConfiguration;

public abstract class ACfg extends ConfiguredObject {

	protected ACfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	public String getId() {
		return config.getRootElementName();
	}
}
