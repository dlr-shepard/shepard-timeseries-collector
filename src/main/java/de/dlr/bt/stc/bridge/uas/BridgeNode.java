package de.dlr.bt.stc.bridge.uas;

import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.sdk.server.nodes.filters.AttributeFilters;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import de.dlr.bt.stc.bridge.Bridge;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.opcuaserver.ANodeCreator;
import de.dlr.bt.stc.opcuaserver.NodeFactory;
import de.dlr.bt.stc.opcuaserver.STCNamespace;
import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BridgeNode extends ANodeCreator {
	private static final String BRIDGE_TYPE = "BridgeType";

	private static final String SOURCE_ID = "SourceID";
	private static final String SINK_ID = "SinkID";
	private static final String QUEUE_SIZE = "QueueSize";
	private static final String QUEUE_DURATION = "QueueDuration";
	private static final String VALUE_TEMPLATE = "ValueTemplate";
	private static final String CACHE_CLEARED = "CacheCleared";
	private static final String CURRENT_SIZE = "CurrentSize";

	private UaObjectTypeNode typeNode;

	@Register
	public static void register() {
		NodeFactory.getInstance().registerCreator(Bridge.class, BridgeNode::new);
	}

	private BridgeNode(STCNamespace namespace) {
		super(namespace, "stc", "bridge");
	}

	@Override
	public void createObjectType() {
		var sourceid = namespace.createObjectTypeComponent(SOURCE_ID, namespace.newNodeId(nodePathType(SOURCE_ID)),
				Identifiers.String);
		var sinkid = namespace.createObjectTypeComponent(SINK_ID, namespace.newNodeId(nodePathType(SINK_ID)),
				Identifiers.String);
		var queuesize = namespace.createObjectTypeComponent(QUEUE_SIZE, namespace.newNodeId(nodePathType(QUEUE_SIZE)),
				Identifiers.Integer);
		var queueduration = namespace.createObjectTypeComponent(QUEUE_DURATION,
				namespace.newNodeId(nodePathType(QUEUE_DURATION)), Identifiers.Integer);
		var valuetemplate = namespace.createObjectTypeComponent(VALUE_TEMPLATE,
				namespace.newNodeId(nodePathType(VALUE_TEMPLATE)), Identifiers.String);
		var queuefull = namespace.createObjectTypeComponent(CACHE_CLEARED,
				namespace.newNodeId(nodePathType(CACHE_CLEARED)), Identifiers.DateTime);
		var currentsize = namespace.createObjectTypeComponent(CURRENT_SIZE,
				namespace.newNodeId(nodePathType(CURRENT_SIZE)), Identifiers.Integer);

		typeNode = namespace.createObjectTypeNode(BRIDGE_TYPE, namespace.newNodeId(nodePathType(BRIDGE_TYPE)), sourceid,
				sinkid, queuesize, queueduration, valuetemplate, queuefull, currentsize);
	}

	@Override
	public void createInstance(Object forNode, Folders folders) {
		if (!(forNode instanceof Bridge bridge))
			return;

		try {
			String id = bridge.getKey();
			var uon = namespace.createObjectNode(typeNode, id, namespace.newNodeId(nodePathInst(id)),
					folders.getBridgeFolder());

			namespace.setObjectNodeComponent(uon, SOURCE_ID, new Variant(bridge.getSourceKey()));
			namespace.setObjectNodeComponent(uon, SINK_ID, new Variant(bridge.getSinkKey()));
			namespace.setObjectNodeComponent(uon, QUEUE_SIZE, new Variant(bridge.getQueueSize()));
			namespace.setObjectNodeComponent(uon, QUEUE_DURATION, new Variant(bridge.getQueueDuration()));
			namespace.setObjectNodeComponent(uon, VALUE_TEMPLATE, new Variant(bridge.getValueTemplate()));

			var qf = namespace.getObjectNodeComponent(uon, CACHE_CLEARED);
			if (qf != null) {
				qf.getFilterChain().addLast(AttributeFilters
						.getValue(context -> new DataValue(new Variant(new DateTime(bridge.getCacheCleared())))));
			}

			var cs = namespace.getObjectNodeComponent(uon, CURRENT_SIZE);
			if (cs != null) {
				cs.getFilterChain().addLast(
						AttributeFilters.getValue(context -> new DataValue(new Variant(bridge.getCurrentCacheSize()))));
			}

			addRootNode(forNode, uon);
		} catch (UaException e) {
			log.warn("Exception during creation of NodeInstance {}: {}", bridge.getKey(), e);
		}

	}

}
