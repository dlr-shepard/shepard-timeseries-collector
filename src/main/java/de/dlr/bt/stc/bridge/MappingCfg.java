package de.dlr.bt.stc.bridge;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.ConfiguredObject;

public class MappingCfg extends ConfiguredObject {

	public MappingCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	public String getMeasurement() {
		return config.getString("measurement");
	}

	public String getLocation() {
		return config.getString("location");
	}

	public String getDevice() {
		return config.getString("device");
	}

	public String getSymbolicName() {
		return config.getString("symbolic_name");
	}

	// Field is optional when configured at sink level
	public String getField() {
		return config.getString("field", "");
	}

}
