package de.dlr.bt.stc.eventbus;

import java.util.List;

import de.dlr.bt.stc.entities.DataPoint;
import de.dlr.bt.stc.entities.Mapping;
import lombok.ToString;
import lombok.Value;

@Value
public class CacheFullEvent {

	private final String sinkKey;
	@ToString.Exclude
	private final List<DataPoint> data;
	private final Mapping timeseries;

}
