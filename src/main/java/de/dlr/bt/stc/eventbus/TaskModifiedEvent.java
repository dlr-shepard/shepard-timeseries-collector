package de.dlr.bt.stc.eventbus;

import de.dlr.bt.stc.task.ITask;
import lombok.Value;

@Value
public class TaskModifiedEvent {
	private ITask task;
	private TaskModificationType modification;

	public enum TaskModificationType {
		INITIALIZE, START, STOP, REMOVE
	}
}
