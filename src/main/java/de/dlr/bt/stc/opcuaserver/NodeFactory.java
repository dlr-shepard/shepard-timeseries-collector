package de.dlr.bt.stc.opcuaserver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class NodeFactory {
	private static NodeFactory instance;

	private final Map<Class<?>, Function<STCNamespace, INodeCreator>> creators = new HashMap<>();

	private NodeFactory() {
	}

	public static synchronized NodeFactory getInstance() {
		if (instance == null)
			instance = new NodeFactory();
		return instance;
	}

	public <T> void registerCreator(Class<T> clazz, Function<STCNamespace, INodeCreator> creator) {
		creators.put(clazz, creator);
	}

	public Map<Class<?>, INodeCreator> getCreatorInstances(STCNamespace namespace) {
		Map<Class<?>, INodeCreator> results = new HashMap<>();
		for (var entry : creators.entrySet()) {
			INodeCreator cn = entry.getValue().apply(namespace);
			results.put(entry.getKey(), cn);
		}
		return results;
	}

}
