package de.dlr.bt.stc.opcuaserver;

import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;

public interface INodeCreator {
	void createObjectType();

	void createInstance(Object forNode, Folders folders);

	void removeInstance(Object forNode, Folders folders);
}
