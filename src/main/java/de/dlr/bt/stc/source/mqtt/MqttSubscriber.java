package de.dlr.bt.stc.source.mqtt;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.SourceDataType;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class MqttSubscriber implements IMqttMessageListener {

	private static final long MULTIPLIER_NANO = 1000000000L;

	private final MqttClientWrapper clientWrapper;
	private final EventBus eventBus;
	private final String sourceKey;
	private final int qos;
	private final SourceDataType dataType;

	private final MqttTopic mqttTopic;

	@Builder
	public MqttSubscriber(MqttClientWrapper clientWrapper, EventBus eventBus, String sourceKey, Integer qos,
						  String topic,
						  SourceDataType dataType) throws SourceConfigurationException {
		this.clientWrapper = clientWrapper;
		this.eventBus = eventBus;
		this.sourceKey = sourceKey;
		this.qos = qos;
		this.dataType = dataType;

		this.mqttTopic = new MqttTopic(topic);
	}

	public void start() throws MqttException {
		clientWrapper.subscribe(mqttTopic.getActualTopic(), qos, this);
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws MqttException, STCException {
		if (!mqttTopic.checkTopicMatching(topic))
			// Topic does not match regex and thus is not relevant
			return;

		log.debug("MqttSubscriber: message arrived for topic {} and sourceKey {}", topic, sourceKey);

		var variableMap = mqttTopic.extractVariables(topic);
		Optional.ofNullable(message.getPayload())
				.map(payLoad -> MqttDataConverter.convertPayload(payLoad, dataType))
				.map(obj -> createEvent(obj, variableMap))
				.ifPresent(eventBus::post);
	}

	private DataAvailableEvent createEvent(Object payObj, Map<String, String> variableMap) {
		var instant = ZonedDateTime.now().toInstant();
		long timestamp = instant.getEpochSecond() * MULTIPLIER_NANO + instant.getNano();
		return new DataAvailableEvent(sourceKey, payObj, timestamp, variableMap);
	}
}
