package de.dlr.bt.stc.source.opcua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.entities.TaskLifecycle;
import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.ASourceProvider;
import de.dlr.bt.stc.task.ProviderResult;
import de.dlr.bt.stc.task.Result;
import de.dlr.bt.stc.task.TaskProviderFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MiloSourceProvider extends ASourceProvider<MiloSource> {

	private final EventBus instanceEventBus;

	private final Map<MiloClientCfg, MiloClient> miloClients = new HashMap<>();

	@Register
	public static void register() {
		TaskProviderFactory.getInstance().registerCreator(SourceOPCUACfg.class, MiloSourceProvider::new);
	}

	private MiloSourceProvider(ConfigurationManager config) {
		super(config.getManagementEventBus());
		this.instanceEventBus = config.getInstanceEventBus();

		for (var entry : config.getConfigurations().entrySet()) {
			if (entry.getValue() instanceof SourceOPCUACfg soc) {
				this.taskLifecycles.put(soc.getId(), new TaskLifecycle<>(new MiloSource(soc, this.instanceEventBus)));
				this.miloClients.putIfAbsent(new MiloClientCfg(soc), null);
			}
		}
	}

	@Override
	public ProviderResult initializeTasks() {
		Result miloClientResult = createMiloClients();
		ProviderResult initTasksResult = super.initializeTasks();

		initTasksResult.getTaskResults().add(miloClientResult);

		return initTasksResult;
	}

	@Override
	protected Result initializeTask(TaskLifecycle<MiloSource> task) {
		if (task.getTask().getClient() == null) {
			MiloClientCfg mcfg = new MiloClientCfg(task.getTask().getConfig());
			var mc = miloClients.get(mcfg);
			if (mc != null)
				task.getTask().setClient(mc);
		}
		if (task.getTask().getClient() == null) {
			String taskId = task.getTask().getConfig().getId();
			return Result.error(taskId, new STCException("Could not initialize Milo Client"));
		}
		return super.initializeTask(task);
	}

	private Result createMiloClients() {
		List<Exception> exceptions = new ArrayList<>();
		for (var entry : miloClients.entrySet()) {
			if (entry.getValue() == null) {
				try {
					var client = new MiloClient(entry.getKey());
					entry.setValue(client);
				} catch (SourceConfigurationException sce) {
					log.error("Could not initialize MiloClient {}, Exception {}", entry, sce.getMessage());
					exceptions.add(sce);
				}
			}
		}
		if (exceptions.isEmpty())
			return Result.ok(this.getClass().getName());
		else {
			Exception accumulated = new STCException("Multiple exceptions");
			exceptions.forEach(accumulated::addSuppressed);
			return Result.error("Could not create milo client", accumulated);
		}
	}

	@Override
	public ProviderResult startTasks() {
		ProviderResult result = super.startTasks();

		for (var client : miloClients.values()) {
			if (client != null)
				client.updateSubscriptions();
		}
		return result;
	}

	@Override
	public ProviderResult stopTasks() {
		for (var client : miloClients.values()) {
			if (client != null)
				client.removeSubscriptions();
		}

		return super.stopTasks();
	}

	@Override
	public void cleanupTasks() {
		stopTasks();

		super.cleanupTasks();

		for (var entry : miloClients.entrySet()) {
			if (entry.getValue() != null) {
				entry.getValue().cleanup();
				entry.setValue(null);
			}
		}
	}
}
