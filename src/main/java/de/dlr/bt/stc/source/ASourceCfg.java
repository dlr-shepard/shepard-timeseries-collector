package de.dlr.bt.stc.source;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.ACfg;

public abstract class ASourceCfg extends ACfg {
	protected ASourceCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}
}
