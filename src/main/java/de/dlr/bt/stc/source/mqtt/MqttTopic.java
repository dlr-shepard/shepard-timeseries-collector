package de.dlr.bt.stc.source.mqtt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.tuple.ImmutablePair;

import de.dlr.bt.stc.exceptions.STCException;
import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import lombok.Getter;

public class MqttTopic {

	@Getter
	private final String topic;

	@Getter
	private final String actualTopic;

	private final Pattern regex;

	private final Map<Integer, ImmutablePair<Pattern, String>> templates;

	public MqttTopic(String topic) throws SourceConfigurationException {
		this.topic = topic;
		this.templates = new HashMap<>();

		var topicElements = topic.split("/");
		var actualTopicElements = new ArrayList<String>();
		var regexElements = new ArrayList<String>();
		for (var i = 0; i < topicElements.length; i++) {
			if (topicElements[i].startsWith("{{") && topicElements[i].endsWith("}}")) {
				var template = topicElements[i].substring(2, topicElements[i].length() - 2);
				var templateParts = template.split("\\|");
				if (templateParts.length != 2)
					throw new SourceConfigurationException(
							"Template is invalid, need exactly two parts! Template: " + template);

				Pattern pattern;
				try {
					pattern = Pattern.compile(templateParts[0].trim());
				} catch (PatternSyntaxException e) {
					throw new SourceConfigurationException(
							"Regex pattern invalid! Pattern: " + templateParts[0].trim());
				}

				actualTopicElements.add("+");
				templates.put(i, new ImmutablePair<>(pattern, templateParts[1].trim()));
				regexElements.add("(" + templateParts[0].trim() + ")");
			} else {
				actualTopicElements.add(topicElements[i]);
				regexElements.add(Pattern.quote(topicElements[i]));
			}
		}

		this.regex = Pattern.compile(String.join("/", regexElements));
		this.actualTopic = String.join("/", actualTopicElements);
	}

	public boolean checkTopicMatching(String topic) {
		if (templates.isEmpty())
			// Any topic implicitly matches, when no templates are available
			return true;

		return regex.matcher(topic).matches();
	}

	public Map<String, String> extractVariables(String topic) throws STCException {
		var result = new HashMap<String, String>();
		var topicElements = topic.split("/");
		for (var entry : templates.entrySet()) {
			if (entry.getKey() < 0 || entry.getKey() >= topicElements.length)
				// invalid key, usually this means that the given topic does not match
				throw new STCException("Unexpected topic: " + topic);

			if (!entry.getValue().left.matcher(topicElements[entry.getKey()]).matches())
				// the current topic element does not match the given regex
				throw new STCException("Topic element does not match: " + topic);

			result.put(entry.getValue().right, topicElements[entry.getKey()]);
		}
		return result;
	}

}
