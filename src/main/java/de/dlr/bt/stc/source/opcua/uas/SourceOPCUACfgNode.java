package de.dlr.bt.stc.source.opcua.uas;

import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;

import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.opcuaserver.ANodeCreator;
import de.dlr.bt.stc.opcuaserver.NodeFactory;
import de.dlr.bt.stc.opcuaserver.STCNamespace;
import de.dlr.bt.stc.opcuaserver.STCNamespace.Folders;
import de.dlr.bt.stc.source.opcua.SourceOPCUACfg;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SourceOPCUACfgNode extends ANodeCreator {
	@Register
	public static void register() {
		NodeFactory.getInstance().registerCreator(SourceOPCUACfg.class, SourceOPCUACfgNode::new);
	}

	private SourceOPCUACfgNode(STCNamespace namespace) {
		super(namespace, "stc", "sources", "sourceopcuacfg");
	}

	private UaObjectTypeNode typeNode;

	private static final String SOURCEOPCUACFG_TYPE = "SourceOPCUACfgType";

	private static final String ENDPOINT = "Endpoint";
	private static final String NODEID = "NodeID";
	private static final String NODEPATH = "NodePath";
	private static final String SAMPLINGINTERVAL = "SamplingInterval";
	private static final String PUBLISHINGINTERVAL = "PublishingInterval";
	private static final String QUEUESIZE = "QueueSize";

	@Override
	public void createObjectType() {
		var endpoint = namespace.createObjectTypeComponent(ENDPOINT, namespace.newNodeId(nodePathType(ENDPOINT)),
				Identifiers.String);
		var nodeid = namespace.createObjectTypeComponent(NODEID, namespace.newNodeId(nodePathType(NODEID)),
				Identifiers.String);
		var nodepath = namespace.createObjectTypeComponent(NODEPATH, namespace.newNodeId(nodePathType(NODEPATH)),
				Identifiers.String);
		var samplinginterval = namespace.createObjectTypeComponent(SAMPLINGINTERVAL,
				namespace.newNodeId(nodePathType(SAMPLINGINTERVAL)), Identifiers.Double);
		var publishinginterval = namespace.createObjectTypeComponent(PUBLISHINGINTERVAL,
				namespace.newNodeId(nodePathType(PUBLISHINGINTERVAL)), Identifiers.Double);
		var queuesize = namespace.createObjectTypeComponent(QUEUESIZE, namespace.newNodeId(nodePathType(QUEUESIZE)),
				Identifiers.Integer);

		typeNode = namespace.createObjectTypeNode("SourceOPCUACfg",
				namespace.newNodeId(nodePathType(SOURCEOPCUACFG_TYPE)), endpoint, nodeid, nodepath, samplinginterval,
				publishinginterval, queuesize);
	}

	@Override
	public void createInstance(Object forNode, Folders folders) {
		if (!(forNode instanceof SourceOPCUACfg sourceOPCUAcfg))
			return;

		try {
			String id = sourceOPCUAcfg.getId();
			var uon = namespace.createObjectNode(typeNode, id, namespace.newNodeId(nodePathInst(id)),
					folders.getConfigFolder());

			namespace.setObjectNodeComponent(uon, ENDPOINT, new Variant(sourceOPCUAcfg.getEndpoint()));
			namespace.setObjectNodeComponent(uon, NODEID, new Variant(sourceOPCUAcfg.getNodeId()));
			namespace.setObjectNodeComponent(uon, NODEPATH, new Variant(sourceOPCUAcfg.getNodePath()));
			namespace.setObjectNodeComponent(uon, SAMPLINGINTERVAL, new Variant(sourceOPCUAcfg.getSamplingInterval()));
			namespace.setObjectNodeComponent(uon, PUBLISHINGINTERVAL,
					new Variant(sourceOPCUAcfg.getPublishingInterval()));
			namespace.setObjectNodeComponent(uon, QUEUESIZE, new Variant(sourceOPCUAcfg.getQueueSize()));

			addRootNode(forNode, uon);
		} catch (UaException e) {
			log.warn("Exception during creation of NodeInstance {}: {}", sourceOPCUAcfg.getId(), e);
		}

	}
}
