package de.dlr.bt.stc.source.mqtt;

import java.util.Objects;

import de.dlr.bt.stc.source.CredentialsCfg;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class MqttClientCfg {
	private final SourceMQTTCfg sourceCfg;

	public MqttClientCfg(SourceMQTTCfg sourceCfg) {
		this.sourceCfg = Objects.requireNonNull(sourceCfg);
	}

	@ToString.Include
	public String getEndpoint() {
		return sourceCfg.getEndpoint();
	}

	@EqualsAndHashCode.Include
	public String getEndpointLC() {
		return getEndpoint().toLowerCase();
	}

	@EqualsAndHashCode.Include
	@ToString.Include
	public CredentialsCfg getCredentials() {
		return sourceCfg.getCredentials();
	}

	@EqualsAndHashCode.Include
	@ToString.Include
	public double getConnectTimeout() {
		return sourceCfg.getConnectTimeout();
	}
}
