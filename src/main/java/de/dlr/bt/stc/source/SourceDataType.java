package de.dlr.bt.stc.source;

public enum SourceDataType {
	INTEGER, FLOAT, BOOL, OBJECT, STRING
}