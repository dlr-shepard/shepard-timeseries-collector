package de.dlr.bt.stc.source;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.ConfiguredObject;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@ToString(onlyExplicitlyIncluded = true)
public class CredentialsCfg extends ConfiguredObject {

	public CredentialsCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@EqualsAndHashCode.Include
	@ToString.Include
	public String getUsername() {
		return config.getString("username");
	}

	@EqualsAndHashCode.Include
	@ToString.Include
	public String getPassword() {
		return config.getString("password");
	}

}
