package de.dlr.bt.stc.source.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.ISource;
import de.dlr.bt.stc.task.Result;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class MqttSource implements ISource {

	private final SourceMQTTCfg sourceConfig;
	private final MqttFactory mqttFactory;
	private final EventBus instanceEventBus;
	@Getter
	private final MqttClientWrapper mqttClient;
	@Getter
	private final String taskId;

	private MqttSubscriber mqttSubscriber;

	@Override
	public Result initializeTask() {
		try {
			MqttConfigValidator.validateSourceConfiguration(sourceConfig);

			mqttClient.connect();

			mqttSubscriber = mqttFactory.createMqttSubscriber(mqttClient, sourceConfig, instanceEventBus);
			return Result.ok(taskId);
		} catch (SourceConfigurationException e) {
			String msg = String.format("MQTT Source %s: Problems creating MQTT Source ", getTaskId());
			log.error(msg, e);
			return Result.configInvalid(taskId, new SourceConfigurationException(msg, e));
		} catch (MqttException | RuntimeException e) {
			String msg = String.format("MQTT Source %s: Problems creating MQTT Subscriber ", getTaskId());
			log.error(msg, e);
			return Result.error(taskId, new SourceConfigurationException(msg, e));
		}
	}

	@Override
	public Result startTask() {
		try {
			mqttSubscriber.start();
			return Result.ok(taskId);
		} catch (MqttException | RuntimeException e) {
			return Result.error(taskId, e, "Problems connecting to MQTT TOPIC : " + sourceConfig.getTopic());
		}
	}

	@Override
	public Result stopTask() {
		try {
			mqttClient.close();
			return Result.ok(taskId);
		} catch (MqttException | RuntimeException e) {
			String msg = String.format("MQTT Source %s: Problems disconnecting MQTT Subscriber", getTaskId());
			log.error(msg);
			return Result.error(taskId, e, msg);
		}
	}
}
