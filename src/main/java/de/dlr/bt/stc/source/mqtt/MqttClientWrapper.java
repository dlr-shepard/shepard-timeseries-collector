package de.dlr.bt.stc.source.mqtt;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import de.dlr.bt.stc.exceptions.STCException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MqttClientWrapper implements MqttCallbackExtended, IMqttMessageListener {
	private final List<MqttSubscriber> subscriber = new ArrayList<>();

	@Getter(AccessLevel.PACKAGE)
	private final IMqttClient client;
	private final String clientKey;
	private final MqttConnectOptions connectionOptions;

	public MqttClientWrapper(IMqttClient client, MqttConnectOptions connectionOptions) {
		this.client = client;
		this.clientKey = client.getClientId(); // Todo: check
		this.connectionOptions = connectionOptions;
		client.setCallback(this);
	}

	public void connect() throws MqttException {
		if (!client.isConnected()) {
			client.connect(connectionOptions);
		}
	}

	public void close() throws MqttException {
		client.disconnect();
		client.close();
	}

	public void subscribe(String actualTopic, int qos, MqttSubscriber mqttSubscriber) throws MqttException {
		subscriber.add(mqttSubscriber);
		client.subscribe(actualTopic, qos, this);
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws MqttException, STCException {
		for (var sub : subscriber)
			if (sub.getMqttTopic().checkTopicMatching(topic))
				sub.messageArrived(topic, message);
	}

	@Override
	public void connectionLost(Throwable cause) {
		log.warn("MQTT Source {}: connection lost", clientKey, cause);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		log.debug("MQTT Source {}: delivery complete", clientKey);
	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		if (!reconnect) {
			log.info("MQTT Source {}: connecting complete to {}", clientKey, serverURI);
			return;
		}
		try {
			log.info("MQTT Source {}: reconnecting to {}", clientKey, serverURI);
			// Re-subscribe all registered subscriber
			for (var sub : subscriber)
				client.subscribe(sub.getMqttTopic().getActualTopic(), sub.getQos(), this);
		} catch (MqttException e) {
			log.error("MQTT Source {}: problems reconnecting to {}: {}", clientKey, serverURI, e);
		}
	}

}
