package de.dlr.bt.stc.source.dummy;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.AEndpointSourceCfg;

public class SourceDummyCfg extends AEndpointSourceCfg {

	protected SourceDummyCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("dummy_source", SourceDummyCfg::new);
	}

}
