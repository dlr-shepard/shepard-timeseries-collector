package de.dlr.bt.stc.source.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;

import de.dlr.bt.stc.source.SourceDataType;

public class MqttDataConverter {

	public static Object convertPayload(final byte[] payload, final SourceDataType dataType) {
		ObjectMapper mapper = new ObjectMapper();
		String data = new String(payload, Charsets.UTF_8);
		if (dataType == null)
			return data;
		try {
			return switch (dataType) {
				case BOOL -> Boolean.valueOf(data);
				case FLOAT -> Double.valueOf(data);
				case INTEGER -> Long.valueOf(data);
				case OBJECT -> mapper.readValue(data, Object.class);
				default -> data;
			};
		} catch (NumberFormatException | JsonProcessingException e) {
			throw new RuntimeException("Problems converting message payload. ", e);
		}
	}
}
