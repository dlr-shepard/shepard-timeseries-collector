package de.dlr.bt.stc.source.mqtt;

import javax.annotation.Nullable;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.AEndpointSourceCfg;
import de.dlr.bt.stc.source.SourceDataType;

public class SourceMQTTCfg extends AEndpointSourceCfg {

	protected SourceMQTTCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	private static SourceMQTTCfg createInstance(HierarchicalConfiguration<?> config) {
		return new SourceMQTTCfg(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("mqtt", SourceMQTTCfg::createInstance);
	}

	@Nullable
	public String getTopic() {
		return config.getString("topic", null);
	}

	@Nullable
	public Integer getQos() {
		return config.getInt("qos", 1);
	}

	public SourceDataType getDatatype() {
		var cdt = config.getString("datatype", "String");
		switch (cdt.toLowerCase()) {
			case "int", "integer":
				return SourceDataType.INTEGER;
			case "float", "double":
				return SourceDataType.FLOAT;
			case "bool", "boolean":
				return SourceDataType.BOOL;
			case "object":
				return SourceDataType.OBJECT;
			default:
				return SourceDataType.STRING;
		}
	}

}
