package de.dlr.bt.stc.source.rsi;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RSIClientCfg {
	private final SourceRSICfg config;

	public RSIClientCfg(SourceRSICfg config) {
		this.config = config;
	}

	@ToString.Include
	@EqualsAndHashCode.Include
	public int getPort() {
		return config.getPort();
	}
}
