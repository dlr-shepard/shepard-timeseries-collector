package de.dlr.bt.stc.source.dummy;

import java.util.Collections;
import java.util.Random;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.eventbus.DataAvailableEvent;
import de.dlr.bt.stc.source.ISource;
import de.dlr.bt.stc.task.Result;
import de.dlr.bt.stc.util.DateHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummySource extends Thread implements ISource {

	private int loopTime = 2000;
	private boolean running = false;
	private final EventBus eventBus;
	private final Random random = new Random();
	private final String key;

	private final DateHelper dateHelper = new DateHelper();

	public DummySource(String key, EventBus eventBus) {
		this.key = key;
		this.eventBus = eventBus;
	}

	@Override
	public void run() {
		running = true;
		while (running) {
			var value = random.nextDouble(-100, 100);
			var timestamp = dateHelper.getDate().getTime() * (long) 1e6;
			var event = new DataAvailableEvent(key, value, timestamp, Collections.emptyMap());
			log.info("Send event from source {} with data: {} - {}", key, timestamp, value);
			eventBus.post(event);

			try {
				sleep(loopTime);
			} catch (InterruptedException e) {
				currentThread().interrupt();
			}

		}
	}

	@Override
	public Result initializeTask() {
		log.debug("Initializing source {}", key);
		return Result.ok(key);
	}

	@Override
	public Result startTask() {
		try {
			log.debug("Starting source {}", key);
			this.start();
			return Result.ok(this.getClass().getName());
		} catch (RuntimeException e) {
			return Result.error(this.getClass().getName(), e);
		}
	}

	@Override
	public Result stopTask() {
		log.debug("Stopping source {}", key);
		running = false;
		return Result.ok(this.getClass().getName());
	}

}
