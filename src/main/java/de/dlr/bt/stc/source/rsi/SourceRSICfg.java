package de.dlr.bt.stc.source.rsi;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.source.ASourceCfg;
import de.dlr.bt.stc.source.SourceDataType;

public class SourceRSICfg extends ASourceCfg {

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("rsi", SourceRSICfg::new);
	}

	SourceRSICfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	public int getPort() {
		return config.getInt("port", 0);
	}

	public String getPath() {
		return config.getString("path", "/");
	}

	public SourceDataType getDatatype() {
		var cdt = config.getString("datatype", "String");
		switch (cdt.toLowerCase()) {
			case "int", "integer":
				return SourceDataType.INTEGER;
			case "float", "double":
				return SourceDataType.FLOAT;
			case "bool", "boolean":
				return SourceDataType.BOOL;
			default:
				return SourceDataType.STRING;
		}
	}

}
