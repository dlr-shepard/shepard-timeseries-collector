package de.dlr.bt.stc.source.mqtt;

import java.util.Optional;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.exceptions.SourceConfigurationException;
import de.dlr.bt.stc.source.CredentialsCfg;

public class MqttFactory {

	private static MqttFactory instance;

	private MqttFactory() {
	}

	public static synchronized MqttFactory getInstance() {
		if (instance == null)
			instance = new MqttFactory();
		return instance;
	}

	public MqttSubscriber createMqttSubscriber(MqttClientWrapper clientWrapper, SourceMQTTCfg sourceConfig,
											   EventBus eventBus) throws SourceConfigurationException {
		return MqttSubscriber.builder()
				.clientWrapper(clientWrapper)
				.eventBus(eventBus)
				.sourceKey(sourceConfig.getId())
				.topic(sourceConfig.getTopic())
				.qos(sourceConfig.getQos())
				.dataType(sourceConfig.getDatatype())
				.build();
	}

	public MqttClientWrapper createMqttClient(String subscriberId, String endpoint, MqttConnectOptions connectOptions)
			throws MqttException {
		// see https://stackoverflow.com/a/71738755
		var client = new MqttClient(endpoint, subscriberId, new MemoryPersistence());
		return new MqttClientWrapper(client, connectOptions);
	}

	public MqttConnectOptions createMqttClientOptions(MqttClientCfg sourceConfig) {
		var options = new MqttConnectOptions();
		options.setAutomaticReconnect(true);
		options.setCleanSession(false);
		options.setConnectionTimeout((int) sourceConfig.getConnectTimeout());
		setCredentials(sourceConfig.getCredentials(), options);
		return options;
	}

	private void setCredentials(CredentialsCfg credentials, MqttConnectOptions options) {
		if (credentials == null) {
			return;
		}
		options.setUserName(credentials.getUsername());
		options.setPassword(
				Optional.ofNullable(credentials.getPassword())
						.map(p -> p.toCharArray())
						.orElse(null));
	}
}
