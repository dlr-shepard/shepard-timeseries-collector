package de.dlr.bt.stc.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Value;

@Value
public class TaskProviderSet {
	private List<TaskProviderStateManager> sources = new ArrayList<>();
	private List<TaskProviderStateManager> bridges = new ArrayList<>();
	private List<TaskProviderStateManager> sinks = new ArrayList<>();

	public void addSource(TaskProviderStateManager source) {
		sources.add(source);
	}

	public void addBridge(TaskProviderStateManager bridge) {
		bridges.add(bridge);
	}

	public void addSink(TaskProviderStateManager sink) {
		sinks.add(sink);
	}

	public List<ProviderResult> getAllResults() {
		var results = new ArrayList<ProviderResult>();
		results.addAll(sources.stream().map(TaskProviderStateManager::getResults).flatMap(Collection::stream).toList());
		results.addAll(bridges.stream().map(TaskProviderStateManager::getResults).flatMap(Collection::stream).toList());
		results.addAll(sinks.stream().map(TaskProviderStateManager::getResults).flatMap(Collection::stream).toList());
		return results;
	}

	public void addTaskProvider(TaskProviderStateManager provider) {
		switch (provider.getProvider()) {
			case ISourceProvider isp -> addSource(provider);
			case IBridgeProvider ibp -> addBridge(provider);
			case ISinkProvider isp -> addSink(provider);
		}
	}
}
