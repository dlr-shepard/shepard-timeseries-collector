package de.dlr.bt.stc.task;

public enum ResultStatus {
	OK, ERROR, CONFIG_INVALID, CANCELED
}
