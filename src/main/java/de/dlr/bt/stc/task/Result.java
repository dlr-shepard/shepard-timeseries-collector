package de.dlr.bt.stc.task;

import org.apache.commons.configuration2.ex.ConfigurationException;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(builderMethodName = "internalBuilder", access = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {

	private String id;

	private ResultStatus status;

	private String errorMsg;

	private Throwable exception;

	private static ResultBuilder builder(String id) {
		return internalBuilder()
				.id(id);
	}

	public static Result ok(String id) {
		return builder(id)
				.status(ResultStatus.OK)
				.build();
	}

	public static Result configInvalid(String id, ConfigurationException exception) {
		Result error = configInvalid(id);
		error.setException(exception);
		error.setErrorMsg(exception.getMessage());
		return error;
	}

	public static Result configInvalid(String id) {
		return builder(id)
				.status(ResultStatus.CONFIG_INVALID)
				.build();
	}

	public static Result canceled(String id) {
		return builder(id)
				.status(ResultStatus.CANCELED)
				.build();
	}

	public static Result error(String id, Throwable exception, String additionalErrorMessage) {
		Result error = error(id, exception);
		error.setErrorMsg(additionalErrorMessage);
		return error;
	}

	public static Result error(String id, Throwable exception) {
		Result error = error(id);
		error.setException(exception);
		error.setErrorMsg(exception.getMessage());
		return error;
	}

	public static Result error(String id) {
		return builder(id)
				.status(ResultStatus.ERROR)
				.build();
	}

	public boolean isOk() {
		return ResultStatus.OK == status;
	}

	public boolean isNotOk() {
		return ResultStatus.OK != status;
	}
}
