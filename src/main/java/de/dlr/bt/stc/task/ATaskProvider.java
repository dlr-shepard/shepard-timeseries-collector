package de.dlr.bt.stc.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.entities.TaskLifecycle;
import de.dlr.bt.stc.eventbus.TaskModifiedEvent;
import de.dlr.bt.stc.eventbus.TaskModifiedEvent.TaskModificationType;
import de.dlr.bt.stc.exceptions.STCException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ATaskProvider<T extends ITask> {
	protected final Map<String, TaskLifecycle<T>> taskLifecycles = new HashMap<>();
	protected final EventBus managementEventBus;

	protected ATaskProvider(EventBus managementEventBus) {
		this.managementEventBus = managementEventBus;
	}

	public ProviderResult initializeTasks() {
		ProviderResult result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.INITIALIZE);

		List<Result> taskResults = taskLifecycles.values()
				.stream()
				.map(this::initializeTask)
				.toList();

		result.getTaskResults().addAll(taskResults);

		return result;
	}

	public ProviderResult startTasks() {
		List<Result> startTaskResults = taskLifecycles.values()
				.stream()
				.filter(TaskLifecycle::isInitialized)
				.map(this::startTask)
				.toList();

		ProviderResult result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.START);
		result.getTaskResults().addAll(startTaskResults);
		return result;
	}

	public ProviderResult stopTasks() {
		List<Result> stopTaskResults = taskLifecycles.values()
				.stream()
				.filter(TaskLifecycle::isStarted)
				.map(this::stopTask)
				.toList();
		ProviderResult result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.STOP);
		result.getTaskResults().addAll(stopTaskResults);
		return result;
	}

	public void cleanupTasks() {
		for (var task : taskLifecycles.values())
			cleanupTask(task);
	}

	public Map<String, ITask> getTasks() {
		return taskLifecycles.entrySet().stream()
				.collect(Collectors.toMap(Entry::getKey, entry -> entry.getValue().getTask()));
	}

	protected Result initializeTask(TaskLifecycle<T> task) {
		if (task.isInitialized()) {
			// already initialized
			return Result.ok(this.getClass().getSimpleName());
		}

		Result taskResult = task.getTask().initializeTask();
		if (taskResult.isNotOk()) {
			task.setInitialized(false);
			return taskResult;
		}

		managementEventBus.post(new TaskModifiedEvent(task.getTask(), TaskModificationType.INITIALIZE));
		task.setInitialized(true);
		return taskResult;
	}

	protected Result startTask(TaskLifecycle<T> task) {
		if (!task.isInitialized()) {
			// Return error when task is not yet initialized
			return Result.error(this.getClass().getSimpleName(), new STCException("Task not initialized"));
		} else if (task.isStarted()) {
			// Return ok when task is already started
			return Result.ok(this.getClass().getSimpleName());
		}

		Result result = task.getTask().startTask();
		if (result.isOk()) {
			managementEventBus.post(new TaskModifiedEvent(task.getTask(), TaskModificationType.START));
			task.setStarted(true);
		}
		return result;
	}

	protected Result stopTask(TaskLifecycle<T> task) {
		Result result = task.getTask().stopTask();
		try {
			task.getTask().joinTask();
		} catch (InterruptedException ie) {
			log.error("ATaskProvider thread was interrupted!");
			Thread.currentThread().interrupt();
		}
		managementEventBus.post(new TaskModifiedEvent(task.getTask(), TaskModificationType.STOP));
		task.setStarted(false);
		return result;
	}

	protected void cleanupTask(TaskLifecycle<T> task) {
		managementEventBus.post(new TaskModifiedEvent(task.getTask(), TaskModificationType.REMOVE));
		task.setInitialized(false);
	}
}
