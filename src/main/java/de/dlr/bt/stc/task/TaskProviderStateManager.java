package de.dlr.bt.stc.task;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class TaskProviderStateManager {
	private final ITaskProvider provider;
	private final Map<ResultAction, ProviderResult> results;

	public TaskProviderStateManager(ITaskProvider provider) {
		this.provider = provider;
		this.results = new EnumMap<>(ResultAction.class);
	}

	public Collection<ProviderResult> getResults() {
		return results.values();
	}

	public boolean initialize() {

		ProviderResult result;
		try {
			result = provider.initializeTasks();
		} catch (RuntimeException ex) {
			result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.INITIALIZE);
			result.getTaskResults().add(Result.error(this.getClass().getSimpleName(), ex));
			log.error("An unexpected RuntimeException occurred during initializeTasks", ex);
		}
		addResult(result);

		return result.isOk();
	}

	public boolean start() {
		ProviderResult result;
		try {
			result = provider.startTasks();
		} catch (RuntimeException ex) {
			result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.START);
			result.getTaskResults().add(Result.error(this.getClass().getSimpleName(), ex));
			log.error("An unexpected RuntimeException occurred during startTasks", ex);
		}
		addResult(result);

		return result.isOk();
	}

	public boolean stop() {

		ProviderResult result;
		try {
			result = provider.stopTasks();
		} catch (RuntimeException ex) {
			result = new ProviderResult(this.getClass().getSimpleName(), ResultAction.STOP);
			result.getTaskResults().add(Result.error(this.getClass().getSimpleName(), ex));
			log.error("An unexpected RuntimeException occurred during stopTasks", ex);
		}
		addResult(result);

		return result.isOk();
	}

	public boolean cleanup() {
		try {
			provider.cleanupTasks();
		} catch (RuntimeException ex) {
			log.error("An unexpected RuntimeException occurred during cleanupTasks", ex);
		}
		// Cleanup always succeeds
		return true;
	}

	private void addResult(ProviderResult result) {
		results.put(result.getAction(), result);
	}

	private String getProviderName() {
		return provider.getClass().getSimpleName();
	}
}
