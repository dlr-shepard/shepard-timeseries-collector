package de.dlr.bt.stc.task;

public interface ITask {
	default Result initializeTask() {
		return Result.ok(this.getClass().getName());
	}

	Result startTask();

	Result stopTask();

	default void joinTask() throws InterruptedException {
	}
}
