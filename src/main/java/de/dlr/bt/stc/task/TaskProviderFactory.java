package de.dlr.bt.stc.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.exceptions.STCException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class TaskProviderFactory {
	private static TaskProviderFactory instance;

	private final Map<Class<?>, Function<ConfigurationManager, ITaskProvider>> creators = new HashMap<>();

	private TaskProviderFactory() {
	}

	public static synchronized TaskProviderFactory getInstance() {
		if (instance == null)
			instance = new TaskProviderFactory();
		return instance;
	}

	public Function<ConfigurationManager, ITaskProvider> getCreator(Class<?> forClass) {
		return creators.get(forClass);
	}

	public void registerCreator(Class<?> forClass, Function<ConfigurationManager, ITaskProvider> creator) {
		creators.put(forClass, creator);
	}

	public ITaskProvider createInstance(Class<?> forClass, ConfigurationManager config) throws STCException {
		if (!creators.containsKey(forClass))
			throw new STCException("Cannot create provider for " + forClass.getName());

		var creator = creators.get(forClass);
		return creator.apply(config);
	}

	public TaskProviderSet createInstances(ConfigurationManager config) {
		Set<Class<?>> cfgClasses = new HashSet<>();
		for (var cfg : config.getConfigurations().values())
			cfgClasses.add(cfg.getClass());

		var providers = new TaskProviderSet();
		for (var cfgClass : cfgClasses) {
			try {
				var providerInstance = createInstance(cfgClass, config);
				providers.addTaskProvider(new TaskProviderStateManager(providerInstance));
			} catch (STCException ste) {
				log.error("Failed to create instance for {}, Exception {}", cfgClass, ste);
			}
		}
		return providers;
	}
}
