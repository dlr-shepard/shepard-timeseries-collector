package de.dlr.bt.stc.task;

public enum ResultAction {
	INITIALIZE, START, STOP
}
