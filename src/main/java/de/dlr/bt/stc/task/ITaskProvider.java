package de.dlr.bt.stc.task;

import java.util.Map;

public sealed interface ITaskProvider permits ISourceProvider, IBridgeProvider, ISinkProvider {
	/**
	 * Initialize all tasks provided by the TaskProvider. Tasks are initialized independently of each other, i.e. a
	 * failing task does not prevent other tasks from being initialized
	 * <p>
	 * This method can be called multiple times and should try to re-initialize tasks that have failed previously or
	 * which have been cleaned up earlier
	 *
	 * @return ProviderResult with informations about the initialized tasks
	 */
	ProviderResult initializeTasks();

	/**
	 * Start all Tasks provided by the TaskProvider
	 * <p>
	 * This method can be called multiple times and should try to start tasks that could not be started previously, or
	 * that have been stopped
	 *
	 * @return ProviderResult with informations about the started tasks
	 */
	ProviderResult startTasks();

	/**
	 * Stop all tasks provided by this TaskProvider
	 */
	ProviderResult stopTasks();

	/**
	 * Cleanup remnants of tasks
	 */
	void cleanupTasks();

	/**
	 * Map of all tasks provided by this TaskProvider
	 *
	 * @return Map&lt;String, ITask&gt; Key is the configured name of the task (e.g. source, sink, ...), the value is
	 * the {@link ITask} responsible for given configuration item.
	 */
	Map<String, ITask> getTasks();
}
