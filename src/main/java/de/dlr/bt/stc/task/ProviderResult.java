package de.dlr.bt.stc.task;

import java.util.ArrayList;
import java.util.List;

import lombok.Value;

@Value
public class ProviderResult {
	String id;

	ResultAction action;

	List<Result> taskResults = new ArrayList<>();

	public boolean isOk() {
		return taskResults.stream().allMatch(Result::isOk);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Name: ");
		sb.append(getId()).append(": ")
				.append(action).append("\n");

		//taskResults.forEach(tr -> sb.append(tr.toString()).append("\n"));

		return sb.toString();
	}
}
