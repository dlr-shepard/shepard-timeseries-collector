package de.dlr.bt.stc.sink.shepard;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.CfgFactory;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.sink.ASinkCfg;

public class SinkShepardCfg extends ASinkCfg {

	protected SinkShepardCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	@Register
	public static void register() {
		CfgFactory.getInstance().registerCreator("shepard", SinkShepardCfg::new);
	}

	public String getHost() {
		return config.getString("host");
	}

	public String getApiKey() {
		return config.getString("api_key");
	}

	public Long getTimeseriesContainerId() {
		return config.getLong("timeseries_container_id");
	}
}
