package de.dlr.bt.stc.sink.dummy;

import org.greenrobot.eventbus.EventBus;

import de.dlr.bt.stc.eventbus.CacheFullEvent;
import de.dlr.bt.stc.sink.ASink;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummySink extends ASink {

	public DummySink(String key, SinkDummyCfg cfg, EventBus eventBus) {
		super(key, eventBus, cfg.getField());
	}

	@Override
	protected void handleEvent(CacheFullEvent event) {
		var fieldToSet = decideForField(event.getTimeseries().getField());
		if (fieldToSet == null) {
			log.error("No field configured for timeseries {} in sink {}", event.getTimeseries(), key);
		}
		StringBuilder builder = new StringBuilder();
		builder.append("Received event in sink ").append(key);
		builder.append(" with ").append(event.getTimeseries()).append(" and selected field ").append(fieldToSet);
		builder.append("\nData received:\n");
		for (var point : event.getData()) {
			builder.append("\t").append(point.getTimestamp()).append(" - ").append(point.getValue()).append("\n");
		}
		log.info(builder.toString());
	}

	@Override
	protected boolean validate() {
		return true;
	}

}