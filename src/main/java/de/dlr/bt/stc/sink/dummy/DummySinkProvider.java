package de.dlr.bt.stc.sink.dummy;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.init.Register;
import de.dlr.bt.stc.sink.ASinkProvider;
import de.dlr.bt.stc.task.TaskProviderFactory;

public class DummySinkProvider extends ASinkProvider<DummySink> {

	@Register
	public static void register() {
		TaskProviderFactory.getInstance().registerCreator(SinkDummyCfg.class, DummySinkProvider::new);
	}

	private DummySinkProvider(ConfigurationManager cfg) {
		super(cfg.getManagementEventBus());
		for (var entry : cfg.getConfigurations().entrySet()) {
			if (entry.getValue() instanceof SinkDummyCfg scfg) {
				putSink(entry.getKey(), new DummySink(entry.getKey(), scfg, cfg.getInstanceEventBus()));
			}
		}
	}

}
