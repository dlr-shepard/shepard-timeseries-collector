package de.dlr.bt.stc.sink.shepard;

import de.dlr.shepard.client.java.ApiClient;
import de.dlr.shepard.client.java.api.TimeseriesContainerApi;

public class ShepardApiFactory {

	private ApiClient getApiClient(String basePath, String apiKey) {
		var apiClient = new ApiClient();
		apiClient.setBasePath(basePath);
		apiClient.setApiKey(apiKey);
		return apiClient;
	}

	public TimeseriesContainerApi getTimeseriesApi(String basePath, String apiKey) {
		return new TimeseriesContainerApi(getApiClient(basePath, apiKey));
	}
}
