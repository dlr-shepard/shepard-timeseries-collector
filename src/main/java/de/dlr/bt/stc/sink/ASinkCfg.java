package de.dlr.bt.stc.sink;

import org.apache.commons.configuration2.HierarchicalConfiguration;

import de.dlr.bt.stc.config.ACfg;

public abstract class ASinkCfg extends ACfg {

	protected ASinkCfg(HierarchicalConfiguration<?> config) {
		super(config);
	}

	public String getField() {
		return config.getString("field", "");
	}
}
