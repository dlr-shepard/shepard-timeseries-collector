package de.dlr.bt.stc.exceptions;

import org.apache.commons.configuration2.ex.ConfigurationException;

import lombok.experimental.StandardException;

@StandardException
public class SourceConfigurationException extends ConfigurationException {
	private static final long serialVersionUID = -3199973784913175484L;
}
