package de.dlr.bt.stc.exceptions;

import lombok.experimental.StandardException;

@StandardException
public class SourceException extends STCException {
	private static final long serialVersionUID = 4379191795616045084L;

}
