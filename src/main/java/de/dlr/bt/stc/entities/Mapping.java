package de.dlr.bt.stc.entities;

import lombok.Value;

@Value
public class Mapping {

	private final String measurement;
	private final String location;
	private final String device;
	private final String symbolicName;
	private final String field;
}
