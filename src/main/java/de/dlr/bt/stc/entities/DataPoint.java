package de.dlr.bt.stc.entities;

import lombok.Value;

@Value
public class DataPoint {
	private final long timestamp;
	private final Object value;
}
