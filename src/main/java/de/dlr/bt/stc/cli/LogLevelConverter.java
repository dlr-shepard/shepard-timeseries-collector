package de.dlr.bt.stc.cli;

import java.util.Locale;

import ch.qos.logback.classic.Level;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.TypeConversionException;

public class LogLevelConverter implements ITypeConverter<Level> {

	@Override
	public Level convert(String value) throws TypeConversionException {
		return switch (value.toLowerCase(Locale.ENGLISH)) {
			case "debug" -> Level.DEBUG;
			case "info" -> Level.INFO;
			case "warn" -> Level.WARN;
			case "error" -> Level.ERROR;
			default -> throw new TypeConversionException("Loglevel needs to be one of debug, info, warn or error!");
		};
	}

}
