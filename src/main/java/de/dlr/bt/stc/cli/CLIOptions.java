package de.dlr.bt.stc.cli;

import ch.qos.logback.classic.Level;
import lombok.Getter;
import picocli.CommandLine.Option;

@Getter
public class CLIOptions {
	@Option(names = {"-c", "--configfolder"}, description = "Configuration folder", defaultValue = "config")
	private String configFolder;

	@Option(names = "--no-opcuaserver", description = "Run OPC/UA server. True by default", defaultValue = "true", negatable = true)
	private boolean runOpcuaServer;
	@Option(names = {"-p", "--serverport"}, description = "OPC/UA server TCP port", defaultValue = "4840")
	private Integer opcuaServerPort;

	@Option(names = {"-h", "--help"}, usageHelp = true, description = "Display help information")
	private boolean usageHelpRequested;

	@Option(names = {"-l",
			"--loglevel"}, description = "Log level (one of debug, info, warn, error)", converter = LogLevelConverter.class)
	private Level logLevel;

	@Option(names = {"--force-headless"}, description = "Force use of headless mode", defaultValue = "false")
	private boolean forceHeadless;
}
