package de.dlr.bt.stc.init;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.reflections.Reflections;
import org.reflections.scanners.Scanners;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ModuleInitializer {
	private ModuleInitializer() {
	}

	public static void initialize() {
		Reflections ref = new Reflections("de.dlr.bt.stc", Scanners.MethodsAnnotated);

		var foundMethods = ref.getMethodsAnnotatedWith(Register.class);

		for (var method : foundMethods) {
			try {
				if (method != null && Modifier.isStatic(method.getModifiers())) {
					method.invoke(null);
					log.debug("Invoked method {}", method);
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				log.error("Failed to invoke {} with exception {}", method, e);
			}
		}
	}
}
