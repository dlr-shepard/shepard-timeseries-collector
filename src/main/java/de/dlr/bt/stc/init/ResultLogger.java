package de.dlr.bt.stc.init;

import java.util.List;

import de.dlr.bt.stc.task.ProviderResult;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResultLogger {

	private ResultLogger() {
	}

	public static void logAllResults(List<ProviderResult> providerResults) {

		logStatistics(providerResults);

		logStatus(providerResults);
	}

	public static void logStatus(List<ProviderResult> providerResults) {
		StringBuilder builder = new StringBuilder("\n--- PROVIDER STATUS ---\n");
		providerResults
				.forEach(providerResult -> builder.append(providerResult.toString()).append("\n"));
		builder.append("--- PROVIDER STATUS ---\n");
		log.info(builder.toString());
	}

	public static void logStatistics(List<ProviderResult> providerResults) {
		var taskResults = providerResults.stream().flatMap(pr -> pr.getTaskResults().stream()).toList();

		int numberOfProviders = taskResults.size();

		long numberOfErrors = 0;
		long numberOfSuccessful = 0;
		long numberOfInvalidConfigs = 0;
		long numberOfCanceled = 0;

		for (var r : taskResults) {
			switch (r.getStatus()) {
				case CANCELED:
					numberOfCanceled++;
					break;
				case CONFIG_INVALID:
					numberOfInvalidConfigs++;
					break;
				case ERROR:
					numberOfErrors++;
					break;
				case OK:
					numberOfSuccessful++;
					break;
				default:
					break;
			}
		}

		StringBuilder builder = new StringBuilder("\n--- PROVIDER STARTING STATISTICS ---\n");
		builder.append("Number of errors         : ").append(numberOfErrors)
				.append("/").append(numberOfProviders).append("\n");
		builder.append("Number of invalid configs: ").append(numberOfInvalidConfigs)
				.append("/").append(numberOfProviders).append("\n");
		builder.append("Number of canceled       : ").append(numberOfCanceled)
				.append("/").append(numberOfProviders).append("\n");
		builder.append("Number of successful     : ").append(numberOfSuccessful)
				.append("/").append(numberOfProviders).append("\n");
		builder.append("--- PROVIDER STARTING STATISTICS ---\n");
		log.info(builder.toString());
	}
}
