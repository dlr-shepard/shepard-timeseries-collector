package de.dlr.bt.stc.init;

import java.nio.file.Path;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import de.dlr.bt.stc.config.ConfigurationManager;
import de.dlr.bt.stc.task.ProviderResult;
import de.dlr.bt.stc.task.TaskProviderFactory;
import de.dlr.bt.stc.task.TaskProviderSet;
import de.dlr.bt.stc.task.TaskProviderStateManager;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class STCInstance {
	private final ConfigurationManager configuration;

	/**
	 * Stores the created TaskProviders. Must only be accessed within a synchronized method!
	 */
	private TaskProviderSet tpfResult = null;

	private final EventBus instanceEventBus;

	private ScheduledExecutorService autoRestartService = null;
	private ScheduledFuture<?> autoRestartTask = null;

	/**
	 * Dqeue holding {@link InstanceEvent} items that need to be processed asynchronously
	 */
	private final Deque<InstanceEvent> events = new ConcurrentLinkedDeque<>();

	@Getter
	private volatile boolean terminate;

	/**
	 * Creates a new Shepard Timeseries Collector (STC) instance
	 *
	 * @param managementEventBus An instance of {@link EventBus} which is used for central management events.
	 */
	public STCInstance(EventBus managementEventBus) {
		this.instanceEventBus = new EventBus();
		this.configuration = new ConfigurationManager(this.instanceEventBus, managementEventBus);

		managementEventBus.register(this);
	}

	private void addConfigurationDirectory(Path path) {
		configuration.addConfigurationPath(path);
	}

	private void unloadConfiguration() {
		stopComponents(false);
		configuration.unloadConfiguration();
	}

	private void loadConfiguration() {
		configuration.loadConfigurations();
	}

	private void startComponents(long restartInterval) {
		if (autoRestartTask != null)
			return;

		var startCompResult = startComponentsInternal();

		if (!startCompResult && restartInterval > 0) {
			autoRestartService = Executors.newSingleThreadScheduledExecutor();
			autoRestartTask = autoRestartService.scheduleWithFixedDelay(this::restartComponents, restartInterval,
					restartInterval, TimeUnit.MILLISECONDS);
		}
	}

	private void restartComponents() {
		var result = startComponentsInternal();
		if (result) {
			log.debug("Restart was successful");
			autoRestartTask.cancel(false);
		}
	}

	private synchronized boolean startComponentsInternal() {
		log.debug("Starting components start");
		if (tpfResult == null)
			tpfResult = TaskProviderFactory.getInstance().createInstances(configuration);

		tpfResult.getSinks().forEach(TaskProviderStateManager::initialize);
		tpfResult.getSinks().forEach(TaskProviderStateManager::start);

		tpfResult.getBridges().forEach(TaskProviderStateManager::initialize);
		tpfResult.getBridges().forEach(TaskProviderStateManager::start);

		tpfResult.getSources().forEach(TaskProviderStateManager::initialize);
		tpfResult.getSources().forEach(TaskProviderStateManager::start);

		var providerResults = tpfResult.getAllResults();
		ResultLogger.logAllResults(providerResults);

		log.debug("Starting components end");

		// Check if all providers started completely
		return providerResults.stream().allMatch(ProviderResult::isOk);
	}

	/**
	 * Stop all components that have been started by this instance.
	 */
	private void stopComponents(boolean terminate) {
		log.info("Stopping all components ...");
		if (autoRestartTask != null) {
			autoRestartTask.cancel(false);
			autoRestartTask = null;
		}
		if (autoRestartService != null) {
			autoRestartService.shutdown();
			autoRestartService = null;
		}

		stopComponentsInternal();

		this.terminate = terminate;
	}

	private synchronized void stopComponentsInternal() {
		if (tpfResult == null)
			return;

		tpfResult.getSources().forEach(TaskProviderStateManager::stop);
		tpfResult.getSources().forEach(TaskProviderStateManager::cleanup);

		tpfResult.getBridges().forEach(TaskProviderStateManager::stop);
		tpfResult.getBridges().forEach(TaskProviderStateManager::cleanup);

		tpfResult.getSinks().forEach(TaskProviderStateManager::stop);
		tpfResult.getSinks().forEach(TaskProviderStateManager::cleanup);

		ResultLogger.logAllResults(tpfResult.getAllResults());

		// Delete the reference to all task providers so that a new TaskProviderSet will
		// be created when start is called again
		tpfResult = null;
	}

	@Subscribe
	public void onInstanceEvent(InstanceEvent ie) {
		events.addLast(ie);
		synchronized (events) {
			events.notifyAll();
		}
	}

	public void run() {
		while (!terminate) {
			var event = events.pollFirst();
			if (event != null) {
				event.handle(this);
			} else {
				synchronized (events) {
					try {
						events.wait();
					} catch (InterruptedException ie) {
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}

	/**
	 * Super class for all events that trigger actions within the {@link STCInstance}.
	 */
	@EqualsAndHashCode
	public abstract static class InstanceEvent {
		protected abstract void handle(STCInstance instance);
	}

	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class StartEvent extends InstanceEvent {
		private long autoRestartTime;

		@Override
		protected void handle(STCInstance instance) {
			instance.startComponents(getAutoRestartTime());
		}
	}

	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class StopEvent extends InstanceEvent {
		private boolean terminate;

		@Override
		protected void handle(STCInstance instance) {
			instance.stopComponents(isTerminate());
		}
	}

	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class LoadConfiguration extends InstanceEvent {
		@Override
		protected void handle(STCInstance instance) {
			instance.loadConfiguration();
		}
	}

	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class UnloadConfiguration extends InstanceEvent {
		@Override
		protected void handle(STCInstance instance) {
			instance.unloadConfiguration();
		}
	}

	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class AddConfigurationFolder extends InstanceEvent {
		private Path folder;

		@Override
		protected void handle(STCInstance instance) {
			instance.addConfigurationDirectory(getFolder());
		}
	}
}
