# Shepard Timeseries Collector (sTC)

The shepard Timeseries Collector aims to collect data from different sources and store them as timeseries data to shepard.

## Main concepts

sTC consists of three main blocks:

- **Sources** are responsible for gathering the data. Currently, sources for OPC/UA, MQTT and KUKA RSI are implemented.

- **Sinks** are responsible to store the data. Currently, a sink for shepard is implemented.

- **Bridges** transfer data form sources to sinks. Single data points can be aggregated into chunks in order to reduce the load for the sink.

Every source can be connected to any sink via a bridge. Data is transferred via an internal event bus that connects all building blocks with each other.

The sTC can also be controlled and monitored via OPC/UA. The image also shows a ReST interface that has not yet been implemented.

![architecture](architecture.png)

## Configuration

sTC is configured using YAML configuration files. By default, all files named `*.yml` in subfolder `config` are considered. Other configuration folders can be specified using the command line interface.

For more details on configuration please refer to the [configuration documentation](configuration.md)

## Running

A Java runtime environment (JRE) with at least version 21 is required. The [Adpoptium JRE/JDK](https://www.adoptium.net) is strongly recommended.

The program is delivered as Java archive (JAR) file and can be run as follows

```txt
java -jar shepard-timeseries-collector.jar
```

There are some command line options available, which can be queried using the `--help` command:

```txt
Usage: <main class> [-h] [--[no-]opcuaserver] [-c=<configFolder>]
                    [-l=<logLevel>] [-p=<opcuaServerPort>]
  -c, --configfolder=<configFolder>
                            Configuration folder
  -h, --help               Display help information
  -l, --loglevel=<logLevel>
                            Log level (one of debug, info, warn, error)
      --[no-]opcuaserver   Run OPC/UA server. True by default
  -p, --serverport=<opcuaServerPort>
                            OPC/UA server TCP port
```

As an alternative, you can also use docker/podman to run the program

```txt
docker run -it --rm -v ./config:/config registry.gitlab.com/dlr-shepard/shepard-timeseries-collector:latest
```

## Monitoring

Besides the obvious functionality of collecting data and transferring it to shepard the sTC also provides advanced monitoring and debugging capabilities.

For more details on monitoring please refer to the [monitoring documentation](monitoring.md).
