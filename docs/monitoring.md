# Monitoring

The sTC provides an OPC/UA server at port 4840, which can be used for monitoring and troubleshooting. This server can be used by any OPC/UA client (e.g. [UaExpert](https://www.unified-automation.com/products/development-tools/uaexpert.html)).

The node structure provided by the OPC/UA server is as follows:

```bash
Root/
├── Objects/
│   ├── STC/
│   │   ├── Configuration/
│   │   │   └── ...
│   │   ├── Bridges/
│   │   │   ├── <bridge_name>/
│   │   │   │   ├── CacheCleared
│   │   │   │   ├── CurrentSize
│   │   │   │   ├── QuereDuration
│   │   │   │   ├── QueueSize
│   │   │   │   ├── SinkID
│   │   │   │   ├── SourceID
│   │   │   │   └── ValueTemplate
│   │   │   └── ...
│   │   ├── Sinks/
│   │   │   └── ...
│   │   ├── Sources/
│   │   │   └── ...
│   │   ├── start
│   │   ├── stop
│   │   ├── quit
│   │   ├── reloadConfiguration
│   │   └── unloadConfiguration
│   └── Server/
│       └── ...
├── Types/
│   └── ...
└── Views/
    └── ...
```

The sTC provides a `Configuration` object that shows the currently loaded configuration.

In addition, there are objects for `bridges`, `sinks` and `sources`, which provide debugging information about the current state of the respective components. This shows information such as the currently cached data that has not yet been transferred to a sink.

Furthermore, there are method nodes that can be used to interact with the sTC. These allow for example starting and stopping the sTC or reloading the configuration.
